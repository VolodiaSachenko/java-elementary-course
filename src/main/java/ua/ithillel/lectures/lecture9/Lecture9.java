package ua.ithillel.lectures.lecture9;

import ua.ithillel.lectures.lecture9.generics.Camera;
import ua.ithillel.lectures.lecture9.generics.Container;
import ua.ithillel.lectures.lecture9.generics.Phone;
import ua.ithillel.lectures.lecture9.generics.Product;

import java.util.ArrayList;
import java.util.List;

public class Lecture9 {
    public static void main(String[] args) {
        Container<Product> container = new Container<>();

        Camera camera = new Camera("Canon", 150);
        Phone phone = new Phone("Samsung", 100);

        container.add(camera);
        container.add(phone);

        List<Product> list = new ArrayList<>();
        list.add(camera);
        System.out.println("find result " + findProduct(list, null));

        List<?> objects = new ArrayList<>();
//        objects.add("s");
    }

    private static <T extends Product> boolean findProduct(List<T> products, T product) {
        return products.contains(product);
    }

    private static <T extends Product> List<T> copy(List<T> products, List<T> products2) {
        products2.addAll(products);
        return products;
    }
}
