package ua.ithillel.lectures.lecture9.generics;

public class Camera extends Product {
    private double pixels;

    public Camera(String name, int price) {
        super(name, price);
    }

    public double getPixels() {
        return pixels;
    }

    public void setPixels(double pixels) {
        this.pixels = pixels;
    }
}
