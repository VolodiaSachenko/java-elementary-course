package ua.ithillel.lectures.lecture19;

public class Lecture19 {
    public static void main(String[] args) {
        Thread thread = new Thread("Thread 1") {
            @Override
            public void run() {
                System.out.println("Running thread" + getName());
            }
        };
        thread.start();

        Runnable runnable = () -> System.out.println("Running runnable...");
        Thread thread1 = new Thread(runnable, "Thread 2");
        thread1.start();

        System.out.println("Running thread " + Thread.currentThread().getName());
    }
}
