package ua.ithillel.lectures.lecture19;

import ua.ithillel.lectures.lecture19.account.Account;
import ua.ithillel.lectures.lecture19.account.InsufficientFundsException;

import java.util.concurrent.TimeUnit;

public class Operations {

    public static void main(String[] args) throws InsufficientFundsException, InterruptedException {
        Account a = new Account(1000);
        a.setName("a");
        Account b = new Account(2000);
        b.setName("b");

        Thread thread = new Thread(() -> {
            System.out.println("Running in thread1");
            try {
                transfer(a, b, 200);
            } catch (InsufficientFundsException | InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();

        System.out.println("Running in main thread");
        transfer(b, a, 500);
    }

    private static void transfer(Account acc1, Account acc2, int amount) throws InsufficientFundsException, InterruptedException {
        if (acc1.getBalance() < amount) {
            throw new InsufficientFundsException("Insufficient funds, less than " + amount);
        }
        acc1.getLock().lock();
        acc2.getLock().lock();
        if (acc1.getLock().tryLock(5, TimeUnit.SECONDS)) {
            Thread.sleep(1000);
            try {

                if (acc2.getLock().tryLock(5, TimeUnit.SECONDS)) {
                    try {
                        acc1.withdraw(amount);
                        acc2.deposit(amount);

                        System.out.println("Transfer successful : ");
                        System.out.println("Account " + acc1.getName() + " balance = " + acc1.getBalance());
                        System.out.println("Account " + acc2.getName() + " balance = " + acc2.getBalance());
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        acc1.getLock().unlock();
                        acc2.getLock().unlock();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                acc1.getLock().unlock();
                acc2.getLock().unlock();
            }
        }
    }
}
