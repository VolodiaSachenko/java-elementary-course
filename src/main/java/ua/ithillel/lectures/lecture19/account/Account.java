package ua.ithillel.lectures.lecture19.account;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Getter
@Setter
public class Account {
    private volatile int balance;
    private volatile String name;
    private Lock lock;

    public Account(int balance) {
        this.balance = balance;
        this.lock = new ReentrantLock();
    }


    public void withdraw(int amount) {
        balance -= amount;
    }

    public void deposit(int amount) {
        balance += amount;
    }
}
