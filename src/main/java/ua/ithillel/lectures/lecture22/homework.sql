/*
Создать таблицу car_rent.roles в которой будут храниться роли пользователей :
• id – primary key
• role – varchar
*/
CREATE TABLE car_rent.roles
(
    id   integer not null
        constraint roles_pk
            primary key,
    role varchar(10)
);

/*
Создать таблицу car_ rent.managers в которой будут храниться менеджеры :
• id – primary key
• name - varchar
• login - varchar
• password - varchar • email – varchar
• role – foreign key to car_ rent.role(id)
*/
CREATE TABLE car_rent.managers
(
    id       integer not null
        constraint managers_pk
            primary key,
    name     varchar(50),
    login    varchar(20),
    password varchar(20),
    email    varchar(50),
    role     integer
        constraint managers_roles_id_fk
            references car_rent.roles
);

/*
Добавить в таблицу car_rent.orders новую колонку
• manager_id - foreign key to car_ rent.managers(id)
*/
ALTER TABLE car_rent.orders
    ADD CONSTRAINT orders_managers_id_fk
        FOREIGN KEY (manager_id) REFERENCES car_rent.managers;

/*
Наполнить таблицы cars, managers, clients, orders данными.
*/
-- Cars 11 - записей
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (5, 'Volvo', 'XC40', 2012, 110);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (6, 'Ferrari', '458', 2020, 120);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (7, 'Porsche', '911', 2021, 100);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (8, 'Mercedes', 'SLS AMG', 2022, 110);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (9, 'Audi', 'R8', 2019, 80);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (10, 'Tesla', 'Model S', 2018, 90);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (11, 'Lexus', 'LFA', 2016, 105);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (12, 'Volkswagen', 'Golf', 2010, 40);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (13, 'Ford', 'Mustang', 2015, 85);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (14, 'Renault', 'Megane', 2011, 30);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price)
VALUES (15, 'Ford', 'Focus', 2012, 20);

-- Managers – 9 записей
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (1, 'Kyle', 'admin', '1111', 'kevin@mail.com', 1);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (2, 'Alex', 'alex', '2222', 'alex@mail.com', 2);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (3, 'Paul', 'paul', '222322', 'paul@mail.com', 2);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (4, 'Boris', 'britva', '333', 'boris@mail.com', 2);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (5, 'Viktor', 'viktor', '11123', 'viktor@mail.com', 1);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (6, 'Igor', 'ig', '34433', 'igor@mail.com', 2);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (7, 'Petro', 'petr', '4455', 'petro@mail.com', 3);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (8, 'Ira', 'iren', '2234', 'ira@mail.com', 3);
INSERT INTO car_rent.managers(id, name, login, password, email, role)
VALUES (9, 'Olga', 'olya', '54321', 'olga@mail.com', 3);

-- Clients – 6 записей
INSERT INTO car_rent.clients(id, name, surname, phone)
VALUES (5, 'Petro', 'Petrov', '+38055044321');
INSERT INTO car_rent.clients(id, name, surname, phone)
VALUES (6, 'Vasyl', 'Gontar', '+38066044321');
INSERT INTO car_rent.clients(id, name, surname, phone)
VALUES (7, 'Ira', 'Yurchenko', '+38093044321');
INSERT INTO car_rent.clients(id, name, surname, phone)
VALUES (8, 'Olga', 'Yakovenko', '+38099124321');
INSERT INTO car_rent.clients(id, name, surname, phone)
VALUES (9, 'Dmytro', 'German', '+38097024321');
INSERT INTO car_rent.clients(id, name, surname, phone)
VALUES (10, 'Denis', 'Prokopenko', '+38098024333');

-- Orders – 5 записей
INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id)
VALUES (2, '2021-01-28', 2, 2, 1);
INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id)
VALUES (3, '2021-11-18', 3, 3, 2);
INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id)
VALUES (4, '2022-03-08', 4, 4, 2);
INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id)
VALUES (5, '2022-04-01', 5, 5, 3);
INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id)
VALUES (6, '2020-03-01', 1, 1, 1);

-- Добавить колонку в таблицу car_rent.cars
ALTER TABLE car_rent.cars
    ADD available boolean;

-- 1) Получить всех менеджеров у которых роль «Админ»
SELECT *
FROM car_rent.managers
WHERE role = 1;

-- 2) Получить все заказы у которых дата исполнения позже 01.28.2022
SELECT *
FROM car_rent.orders
WHERE date > '2022-01-28';

-- 3) Получить автомобиль у которого самая высокая цена
SELECT *
FROM car_rent.cars
WHERE price = (
    SELECT MAX(price)
    FROM car_rent.cars
);

-- 4) Получить количество менеджеров у которых роль “Manager”
-- добавил alias managers_count, для удобства
SELECT COUNT(role) AS managers_count
FROM car_rent.managers
WHERE role = 2;

-- 5) Получить изготовителя и марку всех автомобилей у которых есть заказы в таблице orders
SELECT manufacturer, model
FROM car_rent.cars
         INNER JOIN car_rent.orders
                    ON cars.id = orders.car_id;

-- 6) Получить количество изготовителей автомобилей в таблице cars
SELECT manufacturer, COUNT(manufacturer) AS count
FROM car_rent.cars
GROUP BY manufacturer;

-- 7) Написать транзакцию которая будет добавлять два новых заказа в таблицу car_rent.orders и изменять поля
-- available на false у автомобилей которые указаны в ранее добавленных заказах
BEGIN;
INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id)
VALUES (7, '2022-05-10', 8, 1, 1);
INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id)
VALUES (8, '2022-05-11', 9, 1, 1);
UPDATE car_rent.cars
SET available = FALSE
WHERE id < 7;
COMMIT;