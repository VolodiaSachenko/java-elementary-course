create table car_rent.cars
(
    id           int
        constraint cars_pk
            primary key,
    manufacturer varchar(100),
    model        varchar(50),
    year         int not null,
    price        int not null
);

INSERT INTO car_rent.cars(id, manufacturer, model, year, price) VALUES (2,'Honda','Civic',2012,40);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price) VALUES (3,'BMW','X5',2012,80);
INSERT INTO car_rent.cars(id, manufacturer, model, year, price) VALUES (4,'Opel','Astra',2015,45);

INSERT INTO car_rent.clients(id, name, surname, phone) VALUES (1,'Jack','Black','1234455');
INSERT INTO car_rent.clients(id, name, surname, phone) VALUES (2,'Ivan','Ivanov','1234455');
INSERT INTO car_rent.clients(id, name, surname, phone) VALUES (3,'Oleg','Popov','1234455');
INSERT INTO car_rent.clients(id, name, surname, phone) VALUES (4,'Dmitry','Dragunov','1234455');

SELECT * FROM car_rent.cars;
SELECT * FROM car_rent.clients;

SELECT id,name FROM car_rent.clients WHERE surname = 'Black';

UPDATE car_rent.clients SET name = 'Sergei' WHERE id = 1;
UPDATE car_rent.cars SET year = 2009 WHERE manufacturer like 'Honda';

DELETE FROM car_rent.cars WHERE id = 4;

