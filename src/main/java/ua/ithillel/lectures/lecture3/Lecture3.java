package ua.ithillel.lectures.lecture3;

import ua.ithillel.lectures.lecture3.oop.MyClass;
import ua.ithillel.lectures.lecture3.school.Student;
import ua.ithillel.lectures.lecture3.school.Teacher;

import java.util.List;

public class Lecture3 {
    public static void main(String[] args) {
        System.out.println("Hello world from lecture 3");
        MyClass myClass = new MyClass();
        Student student = new Student();
        student.setAge(18);
        student.setName("Ivan");
        student.setEmail("ivan@gmail.com");

        student.setGroups(List.of("java", "qa"));
        System.out.println(student);

        Teacher teacher = new Teacher();
        teacher.setCompany("Infopulse");
        teacher.setName("Stepan");
        teacher.setAge(30);
        teacher.setEmail("email.email.com");
        teacher.setGroups(List.of("java", "qa"));
        System.out.println(teacher);
    }
}
