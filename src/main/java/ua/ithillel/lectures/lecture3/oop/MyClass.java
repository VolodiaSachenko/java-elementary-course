package ua.ithillel.lectures.lecture3.oop;

public class MyClass {

    int a = 5;

    private void method() {
        a = 6;
    }

    public MyClass() {
        System.out.println("MyClass Constructor");
    }
}
