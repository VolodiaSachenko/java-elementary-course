package ua.ithillel.lectures.lecture3.school;

import java.util.ArrayList;
import java.util.List;

public class Student extends SchoolMember {
    List<String> groups;

    public Student() {
        groups = new ArrayList<>();
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", groups=" + groups +
                '}';
    }
}
