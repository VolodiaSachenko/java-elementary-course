package ua.ithillel.lectures.lecture3.school;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends SchoolMember {
    int experience;
    String company;
    List<String> groups;

    public Teacher() {
        groups = new ArrayList<>();
    }

    public int getExperience() {
        return experience;
    }

    public String getCompany() {
        return company;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", experience=" + experience +
                ", company='" + company + '\'' +
                ", groups=" + groups +
                '}';
    }
}
