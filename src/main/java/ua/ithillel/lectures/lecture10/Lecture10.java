package ua.ithillel.lectures.lecture10;

import ua.ithillel.lectures.lecture10.ecxeption.IncorrectFileNameException;
import ua.ithillel.lectures.lecture10.txtfilereader.TxtFileReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Lecture10 {
    public static void main(String[] args) {
        exceptionExamples();
        checkedException();
        customException();
        tryWithResources();
    }

    private static void tryWithResources() {
        try (FileOutputStream fileOutputStream = new FileOutputStream("src/main/java/ua/ithillel/lectures/lecture10/greetings.txt")) {
            String greetings = "Hello world";
            fileOutputStream.write(greetings.getBytes(StandardCharsets.UTF_8));
            System.out.println("File created");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void customException() {
        TxtFileReader txtFileReader = new TxtFileReader("src/main/java/ua/ithillel/lectures/lecture10/text.txt");
        try {
            System.out.println("File content");
            System.out.println(txtFileReader.readFileContent());
        } catch (IncorrectFileNameException | IOException e) {
            if (e instanceof IncorrectFileNameException) {
                System.out.println("incorrect file name");
            }
            e.printStackTrace();
        }
    }

    private static void checkedException() {
        Path textFilePath = Paths.get("src/main/java/ua/ithillel/lectures/lecture10/text.txt");
        try {
            String fileContent = new String(Files.readAllBytes(textFilePath));
            System.out.println(fileContent);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("finally");
        }
    }

    private static void exceptionExamples() {
        int[] myArray = new int[2];
        myArray[0] = 1;
        System.out.println(Arrays.toString(myArray));
        myArray[1] = 1;

        List<String> myList = new ArrayList<>();
        myList.add("a");
        myList.add("b");
        myList.add("z");
        for (String s : myList) {
            System.out.println(s.toUpperCase());
        }
    }
}
