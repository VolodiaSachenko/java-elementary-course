package ua.ithillel.lectures.lecture10.ecxeption;

public class IncorrectFileNameException extends Exception {

    public IncorrectFileNameException(String message) {
        super(message);
    }
}
