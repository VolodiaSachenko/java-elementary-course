package ua.ithillel.lectures.lecture11;

public class Lecture11 {

    public static void main(String[] args) {
        lecture11Changes();
        je01feature();
    }

    public static void lecture11Changes() {
        System.out.println("lecture11Changes");
    }

    public static void je01feature() {
        System.out.println("JE-01 feature");
    }

    public static void cherryPickChanges() {
        System.out.println("cherryPickChanges");
    }
}
