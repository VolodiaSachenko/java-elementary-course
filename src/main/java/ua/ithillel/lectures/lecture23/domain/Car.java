package ua.ithillel.lectures.lecture23.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Car {
    private int id;
    private String manufacturer;
    private String model;
    private int year;
    private int price;
    private boolean available;
}
