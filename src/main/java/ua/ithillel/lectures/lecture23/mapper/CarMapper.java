package ua.ithillel.lectures.lecture23.mapper;

import ua.ithillel.lectures.lecture23.domain.Car;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarMapper implements DatabaseObjectMapper<Car> {

    @Override
    public Car mapObject(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            return mapCar(resultSet);
        }
        return new Car();
    }

    @Override
    public List<Car> mapObjects(ResultSet resultSet) throws SQLException {
        List<Car> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(mapCar(resultSet));
        }
        return result;
    }

    private Car mapCar(ResultSet resultSet) throws SQLException {
        Car car = new Car();
        car.setId(resultSet.getInt("id"));
        car.setManufacturer(resultSet.getString("manufacturer"));
        car.setModel(resultSet.getString("model"));
        car.setYear(resultSet.getInt("year"));
        car.setPrice(resultSet.getInt("price"));
        car.setAvailable(resultSet.getBoolean("available"));
        return car;
    }
}
