package ua.ithillel.lectures.lecture23;

import ua.ithillel.lectures.lecture23.connection.DatabaseConfig;
import ua.ithillel.lectures.lecture23.domain.Car;
import ua.ithillel.lectures.lecture23.mapper.CarMapper;
import ua.ithillel.lectures.lecture23.mapper.DatabaseObjectMapper;

import java.sql.*;
import java.util.List;

public class Lecture23 {

    public static void main(String[] args) {
        DatabaseConfig config = new DatabaseConfig();

        try {
            Connection connection = config.getConnection();
            Statement statement = connection.createStatement();

            String query = "SELECT * FROM car_rent.cars";
            ResultSet resultSet = statement.executeQuery(query);

            DatabaseObjectMapper<Car> mapper = new CarMapper();

            List<Car> cars = mapper.mapObjects(resultSet);
            cars.forEach(System.out::println);
            resultSet.close();

            String query2 = "SELECT * FROM car_rent.cars WHERE id=1";

            ResultSet resultSet2 = statement.executeQuery(query2);

            Car hondaAccord = mapper.mapObject(resultSet2);

            System.out.println("CAR with id 1 = " + hondaAccord);
            resultSet2.close();

            // Prepared statement

            String sql = "INSERT INTO car_rent.clients (id, name, surname, phone) VALUES (?,?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, 11);
            preparedStatement.setString(2, "Oleg");
            preparedStatement.setString(3, "Olegov");
            preparedStatement.setString(4, "5010101010");

            int rows = preparedStatement.executeUpdate();

            System.out.printf("%d rows added", rows);

            preparedStatement.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
