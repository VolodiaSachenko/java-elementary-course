package ua.ithillel.lectures.lecture7;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Lecture7 {
    public static void main(String[] args) {
        //iterators();
        //lists();
        //queues();
        sets();
    }

    private static void sets() {
        Set<String> mySet = new HashSet<>();
        mySet.add("d");
        mySet.add("b");
        mySet.add("b");
        mySet.add("a");

        System.out.println("Hashset = " + mySet);

        Set<String> myLinkedHashSet = new LinkedHashSet<>();
        myLinkedHashSet.add("d");
        myLinkedHashSet.add("b");
        myLinkedHashSet.add("b");
        myLinkedHashSet.add("a");
        System.out.println("LinkedHashSet " + myLinkedHashSet);

        SortedSet<Integer> sortedSet = new TreeSet<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1, o2);
            }
        });
        sortedSet.add(1);
        sortedSet.add(2);
        sortedSet.add(3);

        System.out.println("sorted set " + sortedSet);
    }

    private static void queues() {
        // FIFO
        Queue<String> queue = new LinkedList<>();
        queue.add("cherry");
        queue.add("banana");
        queue.add("apple");

        while (!queue.isEmpty()) {
            System.out.println(queue.poll() + " ");
        }
        // LIFO
        Queue<String> deQueue = new PriorityQueue<>();
        deQueue.add("cherry");
        deQueue.add("banana");
        deQueue.add("apple");
        while (!deQueue.isEmpty()) {
            System.out.println(deQueue.poll() + " ");
        }

        Deque<String> deque = new LinkedList<>();
        deque.add("Element 1 (Tail)");
        deque.addFirst("Element 2 (Head)");
        deque.addLast("Element 3 (Tail)");
        deque.push("Element 4 (Head)");
        deque.offer("Element 5 (Tail)");

        System.out.println("\n" + deque + "\n");
    }

    private static void lists() {
        List<String> myArrayList = new ArrayList<>();
        List<String> myLinkedList = new LinkedList<>();
    }

    private static void iterators() {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);

        Iterator<Integer> iterator = numbers.iterator();
        while (iterator.hasNext()) {
            int next = iterator.next();
            if (next == 2) {
                iterator.remove();
            }
        }

        System.out.println("number = " + numbers);

        CopyOnWriteArrayList<Integer> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        copyOnWriteArrayList.add(1);
        copyOnWriteArrayList.add(2);
        copyOnWriteArrayList.add(3);
        copyOnWriteArrayList.add(4);
        copyOnWriteArrayList.add(5);

        Iterator<Integer> iterator2 = copyOnWriteArrayList.iterator();
        while (iterator2.hasNext()) {
            int next = iterator2.next();
            if (next == 2) {
                copyOnWriteArrayList.remove((Integer) next);
                copyOnWriteArrayList.add(55);
            }
        }
        System.out.println("copyOnWriteArrayList = " + copyOnWriteArrayList);
    }
}
