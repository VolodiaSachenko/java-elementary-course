package ua.ithillel.lectures.lecture12.decorator;

public class LuxuryCar extends CarDecorator {

    public LuxuryCar(Car car) {
        super(car);
    }

    @Override
    public void create() {
        super.create();
        System.out.println("Adding luxury car features...");
    }
}
