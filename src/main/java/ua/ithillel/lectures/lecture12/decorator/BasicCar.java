package ua.ithillel.lectures.lecture12.decorator;

public class BasicCar implements Car {

    @Override
    public void create() {
        System.out.println("Basic car");
    }
}
