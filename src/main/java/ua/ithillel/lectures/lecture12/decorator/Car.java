package ua.ithillel.lectures.lecture12.decorator;

public interface Car {

    void create();
}
