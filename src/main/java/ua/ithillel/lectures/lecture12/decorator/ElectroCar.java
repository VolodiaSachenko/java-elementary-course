package ua.ithillel.lectures.lecture12.decorator;

public class ElectroCar extends CarDecorator {

    public ElectroCar(Car car) {
        super(car);
    }

    @Override
    public void create() {
        super.create();
        System.out.println("Adding electrocar features...");
    }
}
