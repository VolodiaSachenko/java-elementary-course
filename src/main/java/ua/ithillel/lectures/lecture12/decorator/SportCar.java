package ua.ithillel.lectures.lecture12.decorator;

public class SportCar extends CarDecorator {

    public SportCar(Car car) {
        super(car);
    }

    @Override
    public void create() {
        super.create();
        System.out.println("Adding sport car features...");
    }
}
