package ua.ithillel.lectures.lecture12;

import ua.ithillel.lectures.lecture12.builder.Phone;
import ua.ithillel.lectures.lecture12.chain.ATM;
import ua.ithillel.lectures.lecture12.chain.Currency;
import ua.ithillel.lectures.lecture12.decorator.BasicCar;
import ua.ithillel.lectures.lecture12.decorator.Car;
import ua.ithillel.lectures.lecture12.decorator.LuxuryCar;
import ua.ithillel.lectures.lecture12.decorator.SportCar;
import ua.ithillel.lectures.lecture12.singleton.UserSession;

import java.util.Scanner;

public class Lecture12 {
    public static void main(String[] args) {
        singleTon();
        builder();
        decorator();
        chainOfResponsibility();
    }

    private static void chainOfResponsibility() {
        ATM atm = new ATM();

        while (true) {
            int amount;
            System.out.println("Enter sum to withdraw");
            Scanner scanner = new Scanner(System.in);
            amount = scanner.nextInt();
            if (amount % 10 != 0) {
                System.out.println("Invalid amount");
                return;
            }
            atm.chain.withdraw(new Currency(amount));
        }
    }

    private static void singleTon() {
        UserSession userSession = UserSession.getInstance();
        System.out.println("user name = " + userSession.getUserName());
        System.out.println("user is admin = " + userSession.isAdmin());
    }

    private static void builder() {
        Phone phone = new Phone.PhoneBuilder("CX-50", "Samsung")
                .isUsed(true)
                .pixel(13)
                .storage(64)
                .year(2010)
                .build();
    }

    private static void decorator() {
        Car sportCar = new SportCar(new BasicCar());
        sportCar.create();
        System.out.println("\n****");

        Car sportLuxuryCar = new SportCar(new LuxuryCar(new BasicCar()));
        sportLuxuryCar.create();
        System.out.println("\n****");
    }
}
