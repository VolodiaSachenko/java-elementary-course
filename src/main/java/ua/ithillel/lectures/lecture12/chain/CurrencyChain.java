package ua.ithillel.lectures.lecture12.chain;

public interface CurrencyChain {

    void setNextChain(CurrencyChain nextChain);

    void withdraw(Currency currency);
}
