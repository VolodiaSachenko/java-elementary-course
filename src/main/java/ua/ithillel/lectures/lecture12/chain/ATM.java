package ua.ithillel.lectures.lecture12.chain;

import ua.ithillel.lectures.lecture12.chain.bill.UAH100Bill;
import ua.ithillel.lectures.lecture12.chain.bill.UAH10Bill;
import ua.ithillel.lectures.lecture12.chain.bill.UAH20Bill;
import ua.ithillel.lectures.lecture12.chain.bill.UAH50Bill;

public class ATM {
    public CurrencyChain chain;

    public ATM() {
        this.chain = new UAH100Bill();
        CurrencyChain chain50Bill = new UAH50Bill();
        CurrencyChain chain20Bill = new UAH20Bill();
        CurrencyChain chain10Bill = new UAH10Bill();

        chain.setNextChain(chain50Bill);
        chain.setNextChain(chain20Bill);
        chain.setNextChain(chain10Bill);
    }
}
