package ua.ithillel.lectures.lecture12.chain.bill;

import ua.ithillel.lectures.lecture12.chain.Currency;
import ua.ithillel.lectures.lecture12.chain.CurrencyChain;

public class UAH10Bill implements CurrencyChain {

    private CurrencyChain next;

    @Override
    public void setNextChain(CurrencyChain nextChain) {
        this.next = nextChain;
    }

    @Override
    public void withdraw(Currency currency) {
        if (currency.getAmount() >= 10) {
            int num = currency.getAmount() / 10;
            int remaining = currency.getAmount() % 10;
            System.out.println("Withdrawing " + num + " of 10 bill(s)");
            if (remaining != 0) {
                this.next.withdraw(new Currency(remaining));
            }
        } else {
            this.next.withdraw(currency);
        }
    }
}
