package ua.ithillel.lectures.lecture12.chain.bill;

import ua.ithillel.lectures.lecture12.chain.Currency;
import ua.ithillel.lectures.lecture12.chain.CurrencyChain;

public class UAH50Bill implements CurrencyChain {
    private CurrencyChain next;

    @Override
    public void setNextChain(CurrencyChain nextChain) {
        this.next = nextChain;
    }

    @Override
    public void withdraw(Currency currency) {
        if (currency.getAmount() >= 50) {
            int num = currency.getAmount() / 50;
            int remaining = currency.getAmount() % 50;
            System.out.println("Withdrawing " + num + " of 50 bill(s)");
            if (remaining != 0) {
                this.next.withdraw(new Currency(remaining));
            }
        } else {
            this.next.withdraw(currency);
        }
    }
}
