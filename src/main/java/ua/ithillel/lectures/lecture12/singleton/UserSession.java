package ua.ithillel.lectures.lecture12.singleton;

public class UserSession {

    private String userName;
    private boolean isAdmin;
    //Eager
    private static UserSession instance = new UserSession();

    //lazy way
    private UserSession() {
        this.userName = "Stepan";
        this.isAdmin = true;
    }

    public String getUserName() {
        return userName;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public static UserSession getInstance() {
        if (instance == null) {
            instance = new UserSession();
        }
        return instance;
    }
}
