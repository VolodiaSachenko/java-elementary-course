package ua.ithillel.lectures.lecture12.builder;

public class Phone {

    //required parameters
    private String model;
    private String manufacture;

    //optional parameters
    private boolean isUsed;
    private int storage;
    private int pixel;
    private int year;

    public Phone(PhoneBuilder phoneBuilder) {
        this.model = phoneBuilder.model;
        this.manufacture = phoneBuilder.manufacture;
        this.isUsed = phoneBuilder.isUsed;
        this.storage = phoneBuilder.storage;
        this.pixel = phoneBuilder.pixel;
        this.year = phoneBuilder.year;
    }

    public Phone(String model, String manufacture, boolean isUsed, int storage, int pixel, int year) {
        this.model = model;
        this.manufacture = manufacture;
        this.isUsed = isUsed;
        this.storage = storage;
        this.pixel = pixel;
        this.year = year;
    }

    public static class PhoneBuilder {

        //required parameters
        private String model;
        private String manufacture;

        //optional parameters
        private boolean isUsed;
        private int storage;
        private int pixel;
        private int year;

        public PhoneBuilder(String model, String manufacture) {
            this.model = model;
            this.manufacture = manufacture;
        }

        public PhoneBuilder isUsed(boolean isUsed) {
            this.isUsed = isUsed;
            return this;
        }

        public PhoneBuilder storage(int storage) {
            this.storage = storage;
            return this;
        }

        public PhoneBuilder pixel(int pixel) {
            this.pixel = pixel;
            return this;
        }

        public PhoneBuilder year(int year) {
            this.year = year;
            return this;
        }

        public Phone build() {
            return new Phone(this);
        }
    }
}
