package ua.ithillel.lectures.lecture1;

/**
 * @author Volodia Sachenko
 */

public class Lecture1 {
    public static void main(String[] args) {
        int[] myArray = new int[5];
        String str = "aaa";
        calculateSum(myArray);
        countA(str);
    }

    private static void calculateSum(int[] array) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            counter++;
        }
        System.out.println("Sum is = " + counter);
    }

    private static void countA(String str) {
        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'a')
                counter++;
        }
        System.out.println("A count is = " + counter);
    }
}
