package ua.ithillel.lectures.lecture20;

import ua.ithillel.lectures.lecture19.account.Account;
import ua.ithillel.lectures.lecture20.semaphore.CommonResource;
import ua.ithillel.lectures.lecture20.semaphore.CountThread;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Lecture20 {

    public static void main(String[] args) {
        Account account1 = new Account(2500);
        account1.setName("a");

        Account account2 = new Account(2000);
        account2.setName("b");
        Random random = new Random();

        ExecutorService executorService = Executors.newFixedThreadPool(3);
//        for (int i = 0; i < 10; i++) {
//            executorService.submit(new Transfer(i, account1, account2, random.nextInt(200)));
//        }
//        executorService.shutdown();

//        ScheduledExecutorService executorService1 = Executors.newScheduledThreadPool(3);
//
//        for (int i = 0; i <= 10; i++) {
//            executorService1.schedule(new Transfer(i, account1, account2, random.nextInt(200)), 5, TimeUnit.SECONDS);
//        }
//
//        executorService1.shutdown();

        semaphore();
    }

    private static void semaphore() {
        Semaphore semaphore = new Semaphore(2);
        CommonResource commonResource = new CommonResource();

        new Thread(new CountThread(commonResource, semaphore, "CountThread 1")).start();
        new Thread(new CountThread(commonResource, semaphore, "CountThread 2")).start();
        new Thread(new CountThread(commonResource, semaphore, "CountThread 3")).start();
    }
}
