package ua.ithillel.lectures.lecture20.semaphore;

import java.util.concurrent.Semaphore;

public class CountThread implements Runnable {

    CommonResource resource;
    Semaphore semaphore;
    String name;

    public CountThread(CommonResource resource, Semaphore semaphore, String name) {
        this.resource = resource;
        this.semaphore = semaphore;
        this.name = name;
    }

    @Override
    public void run() {
        try {

            semaphore.acquire();
            System.out.println(name + " acquires resource");
            for (int i = 1; i < 5; i++) {
                System.out.println(this.name + ": " + resource.x);
                resource.x++;
                Thread.sleep(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(name + " releases resource");
        semaphore.release();
    }
}
