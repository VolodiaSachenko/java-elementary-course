package ua.ithillel.lectures.lecture24.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "cars")
public class Car {

    @Id
    private int id;
    @Column(name = "manufacturer")
    private String manufacturer;
    private String model;
    private int year;
    private int price;
    private boolean available;

}
