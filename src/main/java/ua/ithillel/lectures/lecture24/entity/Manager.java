package ua.ithillel.lectures.lecture24.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "managers")
public class Manager {

    @Id
    private int id;
    private String name;
    private String login;
    private String password;
    private String email;

    @Enumerated
    private Role role;

    @OneToMany
    @JoinColumn(name = "manager_id")
    private List<Order> orders;

}
