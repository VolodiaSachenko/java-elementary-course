package ua.ithillel.lectures.lecture24.dao;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.ithillel.lectures.lecture24.entity.Car;
import ua.ithillel.lectures.lecture24.session.HibernateSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CarDAO implements AbstractDAO<Car> {

    @Override
    public Car findById(int id) throws IOException {
        return HibernateSession.getSessionFactory().openSession().get(Car.class, id);
    }

    @Override
    public void save(Car entity) throws IOException {
        Session session = HibernateSession.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.persist(entity);
        tx.commit();
        session.close();
    }

    @Override
    public void update(Car entity) throws IOException {
        Session session = HibernateSession.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.merge(entity);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(Car entity) throws IOException {
        Session session = HibernateSession.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.remove(entity);
        tx.commit();
        session.close();
    }

    @Override
    public List<Car> findAll() throws IOException {
        List<Car> cars = new ArrayList<>();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Car> cq = builder.createQuery(Car.class);
            Root<Car> carRoot = cq.from(Car.class);
            cq.select(carRoot);
            cars = session.createQuery(cq).getResultList();
        } catch (
                HibernateException exception) {
            exception.printStackTrace();
        }
        return cars;
    }
}
