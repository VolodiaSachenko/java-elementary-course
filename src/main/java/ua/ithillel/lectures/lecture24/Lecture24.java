package ua.ithillel.lectures.lecture24;

import ua.ithillel.lectures.lecture24.dao.CarDAO;
import ua.ithillel.lectures.lecture24.entity.Car;
import ua.ithillel.lectures.lecture24.entity.Manager;
import ua.ithillel.lectures.lecture24.entity.Order;
import ua.ithillel.lectures.lecture24.session.HibernateSession;

import java.io.IOException;
import java.util.List;

public class Lecture24 {

    public static void main(String[] args) throws IOException {
        CarDAO carDAO = new CarDAO();
        System.out.println(carDAO.findById(1));

        Car newCar = new Car();
        newCar.setId(25);
        newCar.setManufacturer("BMW");
        newCar.setModel("X7");
        newCar.setPrice(100);
        newCar.setYear(2018);
        // carDAO.save(newCar);

        Manager manager = HibernateSession.getSessionFactory().openSession().get(Manager.class, 4);
        List<Order> orders = manager.getOrders();

        orders.forEach(order -> {
            System.out.println("order id = " + order.getId());
            System.out.println("order date = " + order.getDate());
        });
    }
}
