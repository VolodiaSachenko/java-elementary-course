package ua.ithillel.lectures.lecture24.session;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ua.ithillel.lectures.lecture24.entity.Car;
import ua.ithillel.lectures.lecture24.entity.Client;
import ua.ithillel.lectures.lecture24.entity.Manager;
import ua.ithillel.lectures.lecture24.entity.Order;

import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class HibernateSession {

    private static SessionFactory sessionFactory;

    private HibernateSession() {

    }

    public static SessionFactory getSessionFactory() throws IOException {
        if (Objects.isNull(sessionFactory)) {
            Configuration cfg = new Configuration();
            cfg.addAnnotatedClass(Car.class);
            cfg.addAnnotatedClass(Manager.class);
            cfg.addAnnotatedClass(Client.class);
            cfg.addAnnotatedClass(Order.class);

            Properties p = new Properties();
            p.load(new FileReader("src/main/java/ua/ithillel/lectures/lecture24/hibernate.properties"));

            cfg.setProperties(p);

            sessionFactory = cfg.buildSessionFactory();
        }
        return sessionFactory;
    }

}
