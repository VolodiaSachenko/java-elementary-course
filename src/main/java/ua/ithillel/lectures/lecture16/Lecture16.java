package ua.ithillel.lectures.lecture16;

import ua.ithillel.lectures.lecture16.optional.Computer;
import ua.ithillel.lectures.lecture16.optional.SoundCard;
import ua.ithillel.lectures.lecture16.optional.USB;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lecture16 {
    public static void main(String[] args) {
//        howToCreateStream();
        streamOperations();
        optional();
    }

    private static String makeOddEvenString(Integer integer) {
        return integer % 2 == 0 ? "Even " + integer : "Odd " + integer;
    }

    private static void optional() {
        Computer computer = new Computer();
        SoundCard soundCard = new SoundCard();
        USB usb = new USB();
        usb.setVersion("1.0");
        soundCard.setUsb(usb);
        computer.setSoundCard(soundCard);
        String usbVersion = "UNKNOWN";
        if (computer != null) {
            SoundCard sd = computer.getSoundCard();
            if (sd != null) {
                USB u = sd.getUsb();
                if (u != null) {
                    usbVersion = u.getVersion();
                }
            }
        }

        String optUSBVersion = Optional.ofNullable(computer)
                .flatMap(computer1 -> Optional.ofNullable(computer1.getSoundCard()))
                .flatMap(soundCard1 -> Optional.ofNullable(soundCard1.getUsb()))
                .map(USB::getVersion)
                .orElse("null");
    }

    private static void streamOperations() {
        List<Integer> randomNumber = new Random()
                .ints(100, 0, 101)
                .boxed()
                .filter(integer -> integer > 50)
                .collect(Collectors.toList());
        System.out.println(randomNumber);

//        List<Integer> newList = ra
        List<String> strings = randomNumber.stream()
                .map(Lecture16::makeOddEvenString)
                .collect(Collectors.toList());
        int sum = randomNumber.stream()
                .reduce((a, b) -> a + b)
                .orElse(-1);

        System.out.println("Sum is : " + sum);
        strings.forEach(System.out::println);

        List<String> stringsList = List.of("a", "b", "c");
        stringsList.stream()
                .filter(Objects::nonNull)
                .filter(s -> s.equalsIgnoreCase("a"))
                .sorted()
                .forEach(s -> System.out.println(s));
    }

    private static void howToCreateStream() {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        Stream<Integer> stream = numbers.stream();

        List<Integer> addList = stream.filter(integer -> integer % 2 == 0)
                .collect(Collectors.toList());
        addList.forEach(System.out::println);

        Stream.of("a", "b", "c").forEach(System.out::println);

        Arrays.stream(new int[]{1, 2, 3}).forEach(System.out::println);

        Stream.builder().add("1").add("2").add("3");
    }
}
