package ua.ithillel.lectures.lecture4;

import ua.ithillel.lectures.lecture4.encapsulation.Encapsulation;
import ua.ithillel.lectures.lecture4.immutable.ImmutableStudent;
import ua.ithillel.lectures.lecture4.polymorphism.AudioPlayer;
import ua.ithillel.lectures.lecture4.polymorphism.CDPlayer;
import ua.ithillel.lectures.lecture4.polymorphism.USBPlayer;

import java.util.List;

public class Lecture4 extends Encapsulation {
    public static void main(String[] args) {
        Encapsulation encapsulation = new Encapsulation();
        encapsulation.myPublicInt = 7;

        String[] groups = new String[]{"java", "qa"};
        ImmutableStudent student = new ImmutableStudent("Oleg", "oleg@gmail.com", groups);

        String[] studentGroups = student.getGroups();
        studentGroups[0] = "Python";

        System.out.println(student);

        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play();
        CDPlayer cdPlayer = new CDPlayer();
        cdPlayer.play();
        USBPlayer usbPlayer = new USBPlayer();
        usbPlayer.play();

        List<AudioPlayer> players = List.of(audioPlayer, cdPlayer, usbPlayer);
    }


    public static void play(List<AudioPlayer> players) {
        for (AudioPlayer player : players) {
            player.play();
        }
    }
}
