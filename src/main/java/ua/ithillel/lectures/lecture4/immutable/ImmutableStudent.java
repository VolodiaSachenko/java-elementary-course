package ua.ithillel.lectures.lecture4.immutable;

import java.util.Arrays;

//Declared as final
public final class ImmutableStudent {
    private final String name;
    private final String email;
    private final String[] groups;

    public ImmutableStudent(String name, String email, String[] groups) {
        this.name = name;
        this.email = email;
        this.groups = groups;
    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public String[] getGroups() {
        String[] arrayCopy = new String[this.groups.length];
        System.arraycopy(this.groups, 0, arrayCopy, 0, this.groups.length);
        return arrayCopy;
    }

    @Override
    public String toString() {
        return "ImmutableStudent{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", groups=" + Arrays.toString(groups) +
                '}';
    }
}
