package ua.ithillel.lectures.lecture4.polymorphism;

public class AudioPlayer {
    protected String brand;
    protected String model;

    public void play() {
        System.out.println("Audio player is playing...");
    }
}
