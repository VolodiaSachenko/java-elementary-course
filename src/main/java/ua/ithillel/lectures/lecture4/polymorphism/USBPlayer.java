package ua.ithillel.lectures.lecture4.polymorphism;

public class USBPlayer extends AudioPlayer {
    @Override
    public void play() {
        System.out.println("USB player is playing...");
    }

    // Overloading
    public void play(String song) {

    }
}
