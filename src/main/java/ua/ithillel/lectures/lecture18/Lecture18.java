package ua.ithillel.lectures.lecture18;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import ua.ithillel.lectures.lecture18.json.JSONParser;
import ua.ithillel.lectures.lecture18.nio.FileReader;
import ua.ithillel.lectures.lecture18.xml.DOMParser;
import ua.ithillel.lectures.lecture18.xml.SAXParser;
import ua.ithillel.lectures.lecture18.xml.mapper.BookMapper;
import ua.ithillel.lectures.lecture18.xml.mapper.Mapper;
import ua.ithillel.lectures.lecture18.xml.model.Book;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class Lecture18 {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        nio();
//        domParser();
//        saxParser();
        jsonParser();
    }

    private static void jsonParser() {
        JSONParser jsonParser = new JSONParser();
        jsonParser.parse("src/main/java/ua/ithillel/lectures/lecture18/json/books.json");
    }

    private static void saxParser() throws ParserConfigurationException, IOException, SAXException {
        SAXParser saxParser = new SAXParser();
        saxParser.parsDocument("src/main/java/ua/ithillel/lectures/lecture18/xml/books.xml");
    }

    private static void domParser() throws ParserConfigurationException, IOException, SAXException {
        DOMParser domParser = new DOMParser();
        Document document = domParser.parseDocument("src/main/java/ua/ithillel/lectures/lecture18/xml/books.xml");
        Mapper<Book> bookMapper = new BookMapper();
        bookMapper.getAll(document.getElementsByTagName("book"));
        List<Book> bookList = bookMapper.getAll(document.getElementsByTagName("book"));
        bookList.forEach(System.out::println);
    }

    private static void nio() {
        FileReader fileReader = new FileReader();
        try {
            fileReader.read("src/main/java/ua/ithillel/lectures/lecture18/nio/text.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
