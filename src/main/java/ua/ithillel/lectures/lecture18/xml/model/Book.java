package ua.ithillel.lectures.lecture18.xml.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Book {
    private String id;
    private String author;
    private String title;
    private String genre;
    private double price;
    @SerializedName("publish_date")
    private String publishDate;
    private String description;
}
