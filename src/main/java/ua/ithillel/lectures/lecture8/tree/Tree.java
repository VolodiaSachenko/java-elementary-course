package ua.ithillel.lectures.lecture8.tree;

public class Tree {
    int value;
    private Tree left;
    private Tree right;

    public Tree(int value, Tree left, Tree right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public Tree(int value) {
        this.value = value;
    }

    public void setLeft(Tree left) {
        this.left = left;
    }

    public void setRight(Tree right) {
        this.right = right;
    }

    public int sum() {
        int sum = value;
        if (left != null) {
            sum = left.sum();
        }
        if (right != null) {
            sum = right.sum();
        }
        return sum;
    }
}
