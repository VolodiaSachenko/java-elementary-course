package ua.ithillel.lectures.lecture8;

import ua.ithillel.lectures.lecture8.collision.Employee;
import ua.ithillel.lectures.lecture8.tree.Tree;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Lecture8 {
    public static void main(String[] args) {
        maps();
        equalsHashCodeCollision();
        trees();

    }

    private static void trees() {
        Tree root = new Tree(20);
        Tree rootLeft = new Tree(6);
        Tree rootRight = new Tree(7);

        Tree rooRightChild1 = new Tree(4);
        Tree rooRightChild2 = new Tree(5);
        Tree rooRightChild3 = new Tree(10);


        rooRightChild2.setRight(rooRightChild3);
        rootRight.setLeft(rooRightChild2);
        rootRight.setLeft(rooRightChild1);

        Tree rootLeftChild = new Tree(35);
        rootLeftChild.setRight(new Tree(8));
        rootLeftChild.setLeft(new Tree(9));

        rootLeft.setLeft(rootLeftChild);

        root.setRight(rootRight);
        root.setLeft(rootLeft);

        System.out.println(root.sum());

    }



    private static void equalsHashCodeCollision() {
        Employee employee1 = new Employee(1, "Oleg");
        Employee employee2 = new Employee(1, "Alex");

        Map<Employee, String> employeeMap = new HashMap<>();
        employeeMap.put(employee1, "Onischenko");
        employeeMap.put(employee2, "Ischenko");

        System.out.println(employeeMap.size());
    }

    private static void maps() {
        Map<String, String> birthDayMap = new HashMap<>();
        birthDayMap.put("Oleg", "07.12.1987");
        birthDayMap.put("Kate", "12.12.1987");
        birthDayMap.put("Volodymyr", "13.12.1991");
        System.out.println("Oleg".hashCode());
        for (Map.Entry<String, String> entry : birthDayMap.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }

        Map<String, String> birthDayLinkedHashMap = new LinkedHashMap<>();
        birthDayLinkedHashMap.put("Oleg", "07.12.1987");
        birthDayLinkedHashMap.put("Kate", "12.12.1987");
        birthDayLinkedHashMap.put("Volodymyr", "13.12.1991");
        System.out.println("LinkedHashMap");
        for (Map.Entry<String, String> entry : birthDayLinkedHashMap.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }
}
