package ua.ithillel.lectures.lecture17;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class Lecture17 {
    public static void main(String[] args) {
        Person person1 = new Person("Stepan", "Kachan");
        Person person2 = new Person("Ivan", "Ivanchuk");

        List<Person> persons = List.of(person1, person2);

        try (Writer writer = new FileWriter("persons.json")) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(persons, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
