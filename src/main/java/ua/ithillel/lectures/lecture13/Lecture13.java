package ua.ithillel.lectures.lecture13;

import ua.ithillel.lectures.lecture13.service.ServiceLoader;
import ua.ithillel.lectures.lecture13.service.SimpleService;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Map;

public class Lecture13 {
    public static void main(String[] args) {
        SimpleService service = new SimpleService();

        reflectionFeatures();

        ServiceLoader serviceLoader = new ServiceLoader();
        serviceLoader.loadService("ua.ithillel.lectures.lecture13.service.SimpleService");
        serviceLoader.loadService("ua.ithillel.lectures.lecture13.service.LazyService");
        serviceLoader.loadService("java.lang.String");

        for (Map.Entry<String, Object> entry : serviceLoader.getServiceMap().entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

        serviceLoader.init();
    }

    private static void reflectionFeatures() {
        SimpleService service = new SimpleService();

        Class<? extends SimpleService> aClass = service.getClass();
        Class<SimpleService> simpleServiceClass = SimpleService.class;
        try {
            Class.forName("ua.ithillel.lectures.lecture13.service.SimpleService");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String name = aClass.getName();
        String simpleName = aClass.getSimpleName();
        Class<?> superclass = aClass.getSuperclass();
        Class<?>[] interfaces = aClass.getInterfaces();
        Annotation[] declaredAnnotations = aClass.getDeclaredAnnotations();

        System.out.println("Name " + name);
        System.out.println("Simple name " + simpleName);
        System.out.println("SuperClass " + superclass);
        System.out.println("Interfaces" + Arrays.toString(interfaces));
        System.out.println("Annotations " + Arrays.toString(declaredAnnotations));
    }
}
