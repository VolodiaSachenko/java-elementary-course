package ua.ithillel.lectures.lecture13.service;

import ua.ithillel.lectures.lecture13.annotation.Init;
import ua.ithillel.lectures.lecture13.annotation.Service;

@Service(name = "Simple service")
public class SimpleService {

    @Init(suppressException = true)
    public void initSimpleService() {
        System.out.println("Init simple service");
    }
}
