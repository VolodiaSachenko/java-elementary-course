package ua.ithillel.lectures.lecture13.service;

import ua.ithillel.lectures.lecture13.annotation.Init;
import ua.ithillel.lectures.lecture13.annotation.Service;

@Service(name = "Lazy service", lazyLoad = true)
public class LazyService {

    @Init
    public void initLazyService() {
        System.out.println("Lazy init");
    }

}
