package ua.ithillel.lectures.lecture13.service;

import ua.ithillel.lectures.lecture13.annotation.Init;
import ua.ithillel.lectures.lecture13.annotation.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ServiceLoader {

    private Map<String, Object> serviceMap;

    public ServiceLoader() {
        this.serviceMap = new HashMap<>();
    }

    public void loadService(String className) {
        try {
            Class<?> clazz = Class.forName(className);
            if (clazz.isAnnotationPresent(Service.class)) {
                Object serviceObject = clazz.getConstructor().newInstance();
                String serviceName = clazz.getAnnotation(Service.class).name();
                serviceMap.put(serviceName, serviceObject);
            }
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException |
                InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    public Map<String, Object> getServiceMap() {
        return serviceMap;
    }

    public void init() {
        for (Object service : serviceMap.values()) {
            Class<?> serviceClass = serviceMap.getClass();

            for (Method method : serviceClass.getDeclaredMethods()) {
                if (method.isAnnotationPresent(Init.class)) {
                    try {
                        method.invoke(service);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        Init ann = method.getAnnotation(Init.class);
                        if (ann.suppressException()) {
                            e.printStackTrace();
                        } else {
                            throw new RuntimeException();
                        }
                    }
                }
            }
        }
    }
}
