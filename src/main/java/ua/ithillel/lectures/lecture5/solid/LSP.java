package ua.ithillel.lectures.lecture5.solid;

public class LSP {
    public static void main(String[] args) {
        LSP lsp = new LSP();
        A a = lsp.new A();
        B b = lsp.new B();

        lsp.change(a);
        lsp.change(b);
        System.out.println(a.getName());
        System.out.println(b.getName());
        System.out.println(b.getSurname());
    }

    public void change(A object) {
        if (object instanceof B) {
            ((B) object).setSurname("Changed surname");
        }
        object.setName("Changed name");
    }

    class A {
        String name = "name";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    class B extends A {
        String surname = "surname";

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }
    }
}

