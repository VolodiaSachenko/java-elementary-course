package ua.ithillel.lectures.lecture5;

import ua.ithillel.lectures.lecture5.delivery.manager.DeliveryManager;
import ua.ithillel.lectures.lecture5.delivery.order.CourierOrder;
import ua.ithillel.lectures.lecture5.delivery.order.TaxiOrder;

public class Lecture5 {
    private static String staticString = "ua";

    public static void main(String[] args) {
        DeliveryManager manager = new DeliveryManager();

        CourierOrder courierOrder = new CourierOrder(1, "12.12.21", "Kyiv, Shevhchenko 9");
        CourierOrder courierOrder1 = new CourierOrder(1, "12.12.21", "Kyiv, Shevhchenko 9");

        TaxiOrder taxiOrder = new TaxiOrder(2, "14.12.21", "Lviv", "AA232333BM");
        TaxiOrder taxiOrder2 = new TaxiOrder(2, "14.12.21", "Lviv", "AA232333BM");

        manager.addOrder(courierOrder);
        manager.addOrder(courierOrder1);
        manager.addOrder(taxiOrder);
        manager.addOrder(taxiOrder2);

        manager.showOrders();
        manager.deliverOrder();

        Lecture5.StaticInner staticInner = new StaticInner();
        Lecture5.NonStaticInner nonStaticInner = new Lecture5().new NonStaticInner();
        DaysOfWeek.FRIDAY.ordinal();
        DaysOfWeek.FRIDAY.name();
        DaysOfWeek.values();
    }

    static class StaticInner {

        private void method() {
            staticString = "UA";
        }
    }

    class NonStaticInner {

        private void method() {
            staticString = "UA";
        }
    }
}
