package ua.ithillel.lectures.lecture5.delivery.service;

import ua.ithillel.lectures.lecture5.delivery.order.AbstractOrder;

public interface DeliveryService {
    AbstractOrder deliverOrder(AbstractOrder order);
}
