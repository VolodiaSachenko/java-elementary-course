package ua.ithillel.lectures.lecture5.delivery.order;

import ua.ithillel.lectures.lecture5.delivery.service.DeliveryService;
import ua.ithillel.lectures.lecture5.delivery.service.UberDeliveryService;

public class TaxiOrder extends AbstractOrder {
    private String taxiNumber;

    public TaxiOrder(long id, String date, String address, String taxiNumber) {
        super(id, date, address);
        this.taxiNumber = taxiNumber;
    }

    @Override
    public DeliveryService getDeliveryService() {
        return new UberDeliveryService();
    }
}
