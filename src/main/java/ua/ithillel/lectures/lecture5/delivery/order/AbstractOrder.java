package ua.ithillel.lectures.lecture5.delivery.order;

import ua.ithillel.lectures.lecture5.delivery.service.DeliveryService;

public abstract class AbstractOrder {
    private long id;
    private String date;
    private String address;
    private OrderStatus orderStatus;

    public AbstractOrder(long id, String date, String address) {
        this.id = id;
        this.date = date;
        this.address = address;
        this.orderStatus = OrderStatus.NEW;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public abstract DeliveryService getDeliveryService();
}
