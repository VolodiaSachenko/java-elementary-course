package ua.ithillel.lectures.lecture5.delivery.service;

import ua.ithillel.lectures.lecture5.delivery.order.AbstractOrder;
import ua.ithillel.lectures.lecture5.delivery.order.OrderStatus;

public class GlovoDeliveryService implements DeliveryService {

    @Override
    public AbstractOrder deliverOrder(AbstractOrder order) {
        order.setOrderStatus(OrderStatus.IN_PROGRESS);
        System.out.println("Glovo is delivering order with id " + order.getId());
        try {
            Thread.sleep(3000);
            order.setOrderStatus(OrderStatus.DELIVERED);
            System.out.println("Glovo succesfully delivered order with id " + order.getId());
        } catch (InterruptedException e) {
            System.out.println("Glovo aborted order with id " + order.getId());
            order.setOrderStatus(OrderStatus.ABORTED);
            e.printStackTrace();
        }
        return order;
    }
}
