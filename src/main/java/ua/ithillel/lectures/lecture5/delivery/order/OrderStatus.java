package ua.ithillel.lectures.lecture5.delivery.order;

public enum OrderStatus {
    NEW,
    IN_PROGRESS,
    DELIVERED,
    ABORTED,
    CLOSED;
}
