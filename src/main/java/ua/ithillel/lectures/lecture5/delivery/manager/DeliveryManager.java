package ua.ithillel.lectures.lecture5.delivery.manager;

import ua.ithillel.lectures.lecture5.delivery.order.AbstractOrder;
import ua.ithillel.lectures.lecture5.delivery.service.DeliveryService;

import java.util.ArrayList;
import java.util.List;

public class DeliveryManager {

    List<AbstractOrder> orders;

    public DeliveryManager() {
        this.orders = new ArrayList<>();
    }

    public void addOrder(AbstractOrder order) {
        this.orders.add(order);
    }

    public void showOrders() {
        for (AbstractOrder order : orders) {
            System.out.println(order.getId() + " " + order.getAddress() + " " + order.getDate());
        }
    }

    public void deliverOrder() {
        for (AbstractOrder order : orders) {
            DeliveryService deliveryService = order.getDeliveryService();
            deliveryService.deliverOrder(order);
        }
    }
}
