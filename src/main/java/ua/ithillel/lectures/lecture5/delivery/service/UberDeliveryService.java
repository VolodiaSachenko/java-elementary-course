package ua.ithillel.lectures.lecture5.delivery.service;

import ua.ithillel.lectures.lecture5.delivery.order.AbstractOrder;
import ua.ithillel.lectures.lecture5.delivery.order.OrderStatus;

public class UberDeliveryService implements DeliveryService {

    @Override
    public AbstractOrder deliverOrder(AbstractOrder order) {
        order.setOrderStatus(OrderStatus.IN_PROGRESS);
        System.out.println("Uber is delivering order with id " + order.getId());
        try {
            Thread.sleep(1000);
            order.setOrderStatus(OrderStatus.DELIVERED);
            System.out.println("Uber succesfully delivered order with id " + order.getId());
        } catch (InterruptedException e) {
            System.out.println("Uber aborted order with id " + order.getId());
            order.setOrderStatus(OrderStatus.ABORTED);
            e.printStackTrace();
        }
        return order;
    }
}
