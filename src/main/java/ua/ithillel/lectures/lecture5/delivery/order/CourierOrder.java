package ua.ithillel.lectures.lecture5.delivery.order;

import ua.ithillel.lectures.lecture5.delivery.service.DeliveryService;
import ua.ithillel.lectures.lecture5.delivery.service.GlovoDeliveryService;

public class CourierOrder extends AbstractOrder implements Printable {

    public CourierOrder(long id, String date, String address) {
        super(id, date, address);
    }

    @Override
    public DeliveryService getDeliveryService() {
        return new GlovoDeliveryService();
    }
}
