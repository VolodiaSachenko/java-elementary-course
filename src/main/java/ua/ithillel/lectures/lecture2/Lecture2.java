package ua.ithillel.lectures.lecture2;

public class Lecture2 {

    private static String getDay(int dayNumber) {
        String day;
        switch (dayNumber) {
            case 1 -> day = "Monday";
            case 2 -> day = "Tuesday";
            case 3 -> day = "Wednesday";
            case 4 -> day = "Thursday";
            case 5 -> day = "Friday";
            case 6 -> day = "Saturday";
            case 7 -> day = "Sunday";
            default -> day = "Wrong number";
        }
        return day;
    }

    private static void drawSquare(int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print("# ");
            }
            System.out.println();
        }
    }

    private static void drawSquare2(int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == 0 || j == 0 || i == size - 1 || j == size - 1) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

    private static void drawRectangle(int size) {
        for (int i = 0; i <= size; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("# ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello from lecture 2");
        int a = 5;
        if (a > 5) {
            System.out.println("a > 5");
        } else {
            System.out.println("a < 5");
        }
        drawSquare(5);
        System.out.println(getDay(1));
        drawSquare2(5);
        drawRectangle(5);
    }
}
