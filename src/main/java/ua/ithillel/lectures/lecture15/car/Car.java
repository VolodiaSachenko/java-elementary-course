package ua.ithillel.lectures.lecture15.car;

public class Car {
    private String number;
    private int year;

    public Car() {
        this.number = "";
        this.year = -1;
    }

    public Car(String number, int year) {
        this.number = number;
        this.year = year;
    }

    public void print() {
        System.out.println("Car with number:" + number);
    }

    public String getNumber() {
        return number;
    }

    public int getYear() {
        return year;
    }
}
