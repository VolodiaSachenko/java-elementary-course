package ua.ithillel.lectures.lecture15;

import ua.ithillel.lectures.lecture15.car.Car;
import ua.ithillel.lectures.lecture15.lambda.Greeter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Lecture15 {
    public static void main(String[] args) {
        lambdaExamples();
        examples();
    }

    public void printCar(Supplier<Car> carSupplier) {
        System.out.println(carSupplier.get().getClass().getSimpleName() + " : " + carSupplier.get().getYear() + " : "
                + carSupplier.get().getNumber());
    }

    private static void examples() {
        //Supplier example
        Supplier<Car> carSupplier = () -> new Car("AIDDf66", 2010);
        carSupplier.get();

        //Consumer example
        Consumer<Car> carConsumer = Car::print;
        carConsumer.accept(carSupplier.get());

        //Function example
        Function<Integer, Boolean> is0ddFunction = integer -> integer % 2 == 0;
        System.out.println("Is odd: " + is0ddFunction.apply(11));

        //Predicate example
        Predicate<Car> carYoungerThen2010Predicate = car -> car.getYear() > 2010;
        carYoungerThen2010Predicate.test(new Car("BMW", 2015));

        //Constructor example
        Lecture15 lecture15 = new Lecture15();
        lecture15.printCar(Car::new);

        BiPredicate<Car, Car> carCarBiPredicate = (car, car2) -> car.getYear() > car2.getYear();
    }

    public static boolean isOdd(Integer number) {
        return number % 2 == 0;
    }

    public String myMethod(String str) {
        return str;
    }

    private static void lambdaExamples() {
        Greeter greeter = name -> "Hello " + name;
        System.out.println(greeter.greet("Stepan"));

        Lecture15 lecture15 = new Lecture15();
        Greeter greeter1 = lecture15::myMethod;
    }

    public static void functionalWay() {
        List<Car> cars = Arrays.asList(
                new Car("AA1111BX", 2007),
                new Car("AK223UI", 2000),
                new Car(null, 2012),
                new Car("", 2015),
                new Car("AI1313BM", 2017));

        List<String> carString = findCarsYoungerThan(2010, cars);
        carString.forEach(System.out::println);
        cars.stream()
                .filter(car -> car.getYear() > 2010)
                .filter(car -> Objects.nonNull(car.getNumber()))
                .filter(car -> !car.getNumber().isEmpty())
                .map(Car::getNumber)
                .collect(Collectors.toList());
    }

    public static List<String> findCarsYoungerThan(int year, List<Car> cars) {
        List<String> carYear = new ArrayList<>();
        for (Car car : cars) {
            if (year <= car.getYear()) {
                if (car.getNumber() != null && !car.getNumber().isEmpty())
                    carYear.add(car.getNumber());
            }
        }
        return carYear;
    }
}
