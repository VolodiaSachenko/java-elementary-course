package ua.ithillel.homeworks.homework12.task2;

public class HillelStudent {

    private final String firstName;
    private final String secondName;
    private final StudyGroup group;
    private StudentActivity activity; // This field is important to create a "Strategy" pattern.

    public HillelStudent(String firstName, String secondName, StudyGroup group) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.group = group;
    }

    public void setActivity(StudentActivity activity) {
        this.activity = activity;
    }

    public void executeActivity() {
        activity.setStudentActivity();
    }

    @Override
    public String toString() {
        return "Student: " + firstName + " "
                + secondName + "\n"
                + "group: " + group;
    }
}
