package ua.ithillel.homeworks.homework12.task2;

public enum StudyGroup {

    JAVA("Java"), PHP("PHP"), QA("QA");

    private final String groupName;

    StudyGroup(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return groupName;
    }
}
