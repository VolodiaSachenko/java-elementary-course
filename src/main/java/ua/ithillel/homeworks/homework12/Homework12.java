package ua.ithillel.homeworks.homework12;

import ua.ithillel.homeworks.homework12.task1.Armor;
import ua.ithillel.homeworks.homework12.task1.HairColor;
import ua.ithillel.homeworks.homework12.task1.Hero;
import ua.ithillel.homeworks.homework12.task1.Profession;
import ua.ithillel.homeworks.homework12.task1.Weapon;
import ua.ithillel.homeworks.homework12.task2.HillelStudent;
import ua.ithillel.homeworks.homework12.task2.Offline;
import ua.ithillel.homeworks.homework12.task2.Online;
import ua.ithillel.homeworks.homework12.task2.StudyGroup;

/**
 * @author Volodia Sachenko
 */

public class Homework12 {

    public static void main(String[] args) {

        // Task 1
        Hero hero = new Hero.HeroBuilder("Shiva", 7)
                .profession(Profession.BARBARIAN)
                .weapon(Weapon.MACE)
                .armor(Armor.SHIELD)
                .hairColor(HairColor.BLOND)
                .build();
        Hero hero2 = new Hero.HeroBuilder("Orin", 10)
                .profession(Profession.KNIGHT)
                .weapon(Weapon.SWORD)
                .armor(Armor.SHELL)
                .hairColor(HairColor.BLACK)
                .build();
        Hero hero3 = new Hero.HeroBuilder("Tan", 2)
                .profession(Profession.WARLOCK)
                .weapon(Weapon.BOW)
                .armor(Armor.HELMET)
                .hairColor(HairColor.RED)
                .build();

        System.out.println(hero);
        System.out.println(hero2);
        System.out.println(hero3);

        // Task 2; Strategy Pattern
        HillelStudent student = new HillelStudent("Василь", "Васянович", StudyGroup.JAVA);
        HillelStudent student2 = new HillelStudent("Антон", "Чехов", StudyGroup.QA);
        HillelStudent student3 = new HillelStudent("Тарас", "Шевченко", StudyGroup.PHP);

        System.out.println(student);
        student.setActivity(new Offline());
        student.executeActivity();

        System.out.println("\n" + student2);
        student2.setActivity(new Online());
        student2.executeActivity();

        System.out.println("\n" + student3);
        student3.setActivity(new Offline());
        student3.executeActivity();
    }
}
