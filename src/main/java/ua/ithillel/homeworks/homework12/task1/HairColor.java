package ua.ithillel.homeworks.homework12.task1;

public enum HairColor {

    BLOND("Blond"), BLACK("Black"), RED("Red");

    private final String hairColor;

    HairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    @Override
    public String toString() {
        return hairColor;
    }
}
