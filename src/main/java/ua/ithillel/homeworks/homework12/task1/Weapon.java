package ua.ithillel.homeworks.homework12.task1;

public enum Weapon {

    SWORD("Sword"), BOW("Bow"), MACE("Mace");

    private final String weapon;

    Weapon(String weapon) {
        this.weapon = weapon;
    }

    @Override
    public String toString() {
        return weapon;
    }
}
