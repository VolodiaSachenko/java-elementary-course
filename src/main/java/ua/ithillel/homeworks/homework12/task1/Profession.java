package ua.ithillel.homeworks.homework12.task1;

public enum Profession {

    WARLOCK("Warlock"), KNIGHT("Knight"), BARBARIAN("Barbarian");

    private final String profession;

    Profession(String profession) {
        this.profession = profession;
    }

    @Override
    public String toString() {
        return profession;
    }
}
