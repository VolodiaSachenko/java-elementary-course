package ua.ithillel.homeworks.homework12.task1;

public class Hero {

    private final String name;
    private final int level;
    private final Profession profession;
    private final Weapon weapon;
    private final Armor armor;
    private final HairColor hairColor;

    public Hero(HeroBuilder heroBuilder) {
        this.name = heroBuilder.name;
        this.level = heroBuilder.level;
        this.profession = heroBuilder.profession;
        this.weapon = heroBuilder.weapon;
        this.armor = heroBuilder.armor;
        this.hairColor = heroBuilder.hairColor;
    }

    public static class HeroBuilder {

        private final String name;
        private final int level;
        private Profession profession;
        private Weapon weapon;
        private Armor armor;
        private HairColor hairColor;

        public HeroBuilder(String name, int level) {
            this.name = name;
            this.level = level;
        }

        public HeroBuilder profession(Profession profession) {
            this.profession = profession;
            return this;
        }

        public HeroBuilder weapon(Weapon weapon) {
            this.weapon = weapon;
            return this;
        }

        public HeroBuilder armor(Armor armor) {
            this.armor = armor;
            return this;
        }

        public HeroBuilder hairColor(HairColor hairColor) {
            this.hairColor = hairColor;
            return this;
        }

        public Hero build() {
            return new Hero(this);
        }
    }

    @Override
    public String toString() {
        return "Name of the hero: " + name + "\n" +
                "level: " + level + "\n" +
                "profession: " + profession + "\n" +
                "weapon: " + weapon + "\n" +
                "armor: " + armor + "\n" +
                "hair color: " + hairColor + "\n";
    }
}
