package ua.ithillel.homeworks.homework12.task1;

public enum Armor {

    SHIELD("Shield"), HELMET("Helmet"), SHELL("Shell");

    private final String armor;

    Armor(String armor) {
        this.armor = armor;
    }

    @Override
    public String toString() {
        return armor;
    }
}
