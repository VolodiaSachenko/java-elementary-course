package ua.ithillel.homeworks.homework18;

import jakarta.xml.bind.JAXBException;
import ua.ithillel.homeworks.homework18.mapper.GSONParser;
import ua.ithillel.homeworks.homework18.mapper.XMLSerializer;
import ua.ithillel.homeworks.homework18.model.Car;

import java.io.IOException;
import java.util.List;

/**
 * @author Volodia Sachenko
 */

public class Homework18 {

    public static void main(String[] args) {
        try {
            GSONParser gsonParser = new GSONParser();
            List<Car> cars = gsonParser.parse();
            System.out.println("JSON parsing:");
            cars.forEach(System.out::println);

            //Convert to *.xml
            XMLSerializer.marshalByLocation(cars);
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
    }
}
