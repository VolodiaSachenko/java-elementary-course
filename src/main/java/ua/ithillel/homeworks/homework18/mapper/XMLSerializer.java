package ua.ithillel.homeworks.homework18.mapper;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import ua.ithillel.homeworks.homework18.model.Car;
import ua.ithillel.homeworks.homework18.model.CarsList;

import java.io.File;
import java.util.List;

public class XMLSerializer {

    private final static String XML_PATH = "src/main/resources/carsWithLocation.xml";

    public static void marshalByLocation(List<Car> cars) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(CarsList.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        CarsList catalog = filterCarsByLocation(cars);
        marshaller.marshal(catalog, new File(XML_PATH));
    }

    private static CarsList filterCarsByLocation(List<Car> cars) {
        CarsList catalog = new CarsList();
        catalog.addCarList(cars.stream().filter(car -> car.getLocation() != null).toList());
        return catalog;
    }
}
