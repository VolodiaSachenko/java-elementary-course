package ua.ithillel.homeworks.homework18.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ua.ithillel.homeworks.homework18.model.Car;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GSONParser {

    private final static String JSON_PATH = "src/main/resources/cars.json";

    public List<Car> parse() throws IOException {
        List<Car> carList;
        Reader reader = new FileReader(JSON_PATH);
        Type listType = new TypeToken<ArrayList<Car>>() {
        }.getType();
        carList = new Gson().fromJson(reader, listType);
        reader.close();
        return carList;
    }
}
