package ua.ithillel.homeworks.homework18.model;

import com.google.gson.annotations.SerializedName;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Data;
import ua.ithillel.homeworks.homework18.adapter.DateAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@XmlType(propOrder = {"name", "milesPerGallon", "cylinders", "displacement", "horsepower", "year", "origin", "location"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Car {
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "miles_per_Gallon")
    @SerializedName("miles_per_Gallon")
    private int milesPerGallon;
    @XmlElement(name = "cylinders")
    private int cylinders;
    @XmlElement(name = "displacement")
    private int displacement;
    @XmlElement(name = "horsepower")
    private int horsepower;
    @XmlElement(name = "year", required = true)
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date year;
    @XmlElement(name = "origin")
    private String origin;
    @XmlElement(name = "location")
    private Location location;

    private static DateFormat simpleDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }

    @Override
    public String toString() {
        String display = "name: " + name + "\n" +
                "miles per gallon: " + milesPerGallon + "\n" +
                "cylinders: " + cylinders + "\n" +
                "displacement: " + displacement + "\n" +
                "horsepower: " + horsepower + "\n" +
                "year: " + simpleDateFormat().format(year) + "\n" +
                "origin: " + origin + "\n";
        if (this.getLocation() == null) {
            return display;
        } else {
            return display + location + "\n";
        }
    }
}
