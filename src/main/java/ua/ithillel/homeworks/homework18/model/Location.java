package ua.ithillel.homeworks.homework18.model;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlElement;
import lombok.Data;

@Data
@XmlType(propOrder = {"country", "city", "street", "house"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Location {
    @XmlElement(name = "country")
    private String country;
    @XmlElement(name = "city")
    private String city;
    @XmlElement(name = "street")
    private String street;
    @XmlElement(name = "house")
    private int house;

    @Override
    public String toString() {
        return "location: " + "\n" +
                "country: " + country + "\n" +
                "city: " + city + "\n" +
                "street: " + street + "\n" +
                "house: " + house;
    }
}
