package ua.ithillel.homeworks.homework18.model;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "catalog")
public class CarsList {

    @XmlElement(name = "car")
    public final List<Car> cars = new ArrayList<>();

    public void addCarList(List<Car> cars) {
        this.cars.addAll(cars);
    }
}
