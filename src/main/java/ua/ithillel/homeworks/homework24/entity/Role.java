package ua.ithillel.homeworks.homework24.entity;

public enum Role {
    NULL,
    ADMIN,
    MANAGER,
    CLIENT
}
