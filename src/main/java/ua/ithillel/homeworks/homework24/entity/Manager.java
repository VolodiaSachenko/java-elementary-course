package ua.ithillel.homeworks.homework24.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "managers")
public class Manager {
    @Id
    private int id;
    private String name;
    private String login;
    private String password;
    private String email;
    @Enumerated
    private Role role;
    @ToString.Exclude
    @OneToMany
    @JoinColumn(name = "manager_id")
    private List<Order> orders;
}
