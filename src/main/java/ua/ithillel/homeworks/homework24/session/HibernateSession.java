package ua.ithillel.homeworks.homework24.session;

import lombok.NoArgsConstructor;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ua.ithillel.homeworks.homework24.entity.Car;
import ua.ithillel.homeworks.homework24.entity.Client;
import ua.ithillel.homeworks.homework24.entity.Manager;
import ua.ithillel.homeworks.homework24.entity.Order;
import ua.ithillel.homeworks.homework24.entity.Role;

import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

@NoArgsConstructor
public class HibernateSession {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() throws IOException {
        if (Objects.isNull(sessionFactory)) {
            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass(Car.class);
            configuration.addAnnotatedClass(Client.class);
            configuration.addAnnotatedClass(Manager.class);
            configuration.addAnnotatedClass(Order.class);
            configuration.addAnnotatedClass(Role.class);

            Properties properties = new Properties();
            properties.load(new FileReader("src/main/resources/hibernate.properties"));

            configuration.setProperties(properties);
            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }
}
