package ua.ithillel.homeworks.homework24.dao;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import ua.ithillel.homeworks.homework24.entity.Manager;
import ua.ithillel.homeworks.homework24.session.HibernateSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ManagerDAO implements AbstractDAO<Manager> {

    @Override
    public List<Manager> findAll() throws IOException {
        List<Manager> managers = new ArrayList<>();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Manager> criteriaQuery = criteriaBuilder.createQuery(Manager.class);
            Root<Manager> root = criteriaQuery.from(Manager.class);
            criteriaQuery.select(root);
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));
            managers = session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return managers;
    }

    public List<Manager> findIdLess() throws IOException {
        List<Manager> managers = new ArrayList<>();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Manager> criteriaQuery = criteriaBuilder.createQuery(Manager.class);
            Root<Manager> root = criteriaQuery.from(Manager.class);
            criteriaQuery.select(root).where(criteriaBuilder.lt(root.get("id"), 5));
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));
            managers = session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return managers;
    }
}
