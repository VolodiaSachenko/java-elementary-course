package ua.ithillel.homeworks.homework24.dao;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import ua.ithillel.homeworks.homework24.entity.Client;
import ua.ithillel.homeworks.homework24.session.HibernateSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO implements AbstractDAO<Client> {

    @Override
    public List<Client> findAll() throws IOException {
        List<Client> clients = new ArrayList<>();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Client> criteriaQuery = criteriaBuilder.createQuery(Client.class);
            Root<Client> root = criteriaQuery.from(Client.class);
            criteriaQuery.select(root);
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));
            clients = session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return clients;
    }
}
