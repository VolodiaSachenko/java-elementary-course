package ua.ithillel.homeworks.homework24.dao;

import java.io.IOException;
import java.util.List;

public interface AbstractDAO<T> {

    List<T> findAll() throws IOException;

}
