package ua.ithillel.homeworks.homework24.dao;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import ua.ithillel.homeworks.homework24.entity.Order;
import ua.ithillel.homeworks.homework24.session.HibernateSession;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderDAO implements AbstractDAO<Order> {

    @Override
    public List<Order> findAll() throws IOException {
        List<Order> orders = new ArrayList<>();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Order> criteriaQuery = criteriaBuilder.createQuery(Order.class);
            Root<Order> root = criteriaQuery.from(Order.class);
            criteriaQuery.select(root);
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));
            orders = session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return orders;
    }

    public List<Order> findByDate() throws IOException, ParseException {
        List<Order> orders = new ArrayList<>();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Order> criteriaQuery = criteriaBuilder.createQuery(Order.class);
            Root<Order> root = criteriaQuery.from(Order.class);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse("2022-01-28");
            Predicate greaterThan = criteriaBuilder.greaterThan(root.get("date"), date);
            criteriaQuery.select(root).where(greaterThan);
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));
            orders = session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return orders;
    }
}
