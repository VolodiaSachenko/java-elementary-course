package ua.ithillel.homeworks.homework24.dao;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import ua.ithillel.homeworks.homework24.entity.Car;
import ua.ithillel.homeworks.homework24.session.HibernateSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CarDAO implements AbstractDAO<Car> {

    @Override
    public List<Car> findAll() throws IOException {
        List<Car> cars = new ArrayList<>();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Car> criteriaQuery = criteriaBuilder.createQuery(Car.class);
            Root<Car> root = criteriaQuery.from(Car.class);
            criteriaQuery.select(root);
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));
            cars = session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return cars;
    }

    public Car findMaxPrice() throws IOException {
        Car car = new Car();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Integer> criteriaQuery = criteriaBuilder.createQuery(Integer.class);
            Root<Car> carRoot = criteriaQuery.from(Car.class);
            criteriaQuery.select(criteriaBuilder.max(carRoot.get("price")));
            Integer maxPrice = session.createQuery(criteriaQuery).getSingleResult();
            car = findCarByPrice(maxPrice);
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return car;
    }

    private Car findCarByPrice(Integer price) throws IOException {
        Car car = new Car();
        try (Session session = HibernateSession.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Car> carsWithMaxPrice = criteriaBuilder.createQuery(Car.class);
            Root<Car> findCar = carsWithMaxPrice.from(Car.class);
            carsWithMaxPrice.select(findCar).where(criteriaBuilder.equal(findCar.get("price"), price));
            car = session.createQuery(carsWithMaxPrice).getSingleResult();
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return car;
    }
}
