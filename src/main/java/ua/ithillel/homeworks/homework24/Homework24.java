package ua.ithillel.homeworks.homework24;

import ua.ithillel.homeworks.homework24.dao.ClientDAO;
import ua.ithillel.homeworks.homework24.dao.ManagerDAO;
import ua.ithillel.homeworks.homework24.dao.OrderDAO;
import ua.ithillel.homeworks.homework24.entity.Car;
import ua.ithillel.homeworks.homework24.dao.CarDAO;
import ua.ithillel.homeworks.homework24.entity.Client;
import ua.ithillel.homeworks.homework24.entity.Manager;
import ua.ithillel.homeworks.homework24.entity.Order;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class Homework24 {
    public static void main(String[] args) {
        CarDAO carDAO = new CarDAO();
        ClientDAO clientDAO = new ClientDAO();
        ManagerDAO managerDAO = new ManagerDAO();
        OrderDAO orderDAO = new OrderDAO();
        try {
            List<Car> selectAllCars = carDAO.findAll();
            selectAllCars.forEach(System.out::println);

            List<Client> selectAllClients = clientDAO.findAll();
            selectAllClients.forEach(System.out::println);

            List<Manager> selectAllManagers = managerDAO.findAll();
            selectAllManagers.forEach(System.out::println);

            List<Order> selectOrders = orderDAO.findAll();
            selectOrders.forEach(System.out::println);

            List<Manager> findById = managerDAO.findIdLess();
            System.out.println("\n1) Получить всех менеджеров у которых id меньше 5:");
            findById.forEach(System.out::println);

            List<Order> findByDate = orderDAO.findByDate();
            System.out.println("\n2) Получить все заказы у которых дата исполнения позже 01.28.2022:");
            findByDate.forEach(System.out::println);

            Car car = carDAO.findMaxPrice();
            System.out.println("\n3) Получить автомобиль у которого самая высокая цена:");
            System.out.println(car);
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
