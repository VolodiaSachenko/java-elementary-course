package ua.ithillel.homeworks.homework17;

/**
 * @author Volodia Sachenko
 */

public class Homework17 {

    public static void main(String[] args) {

        Person vasyl = new Person();
        Person viktor = new Person("Viktor", "Prokopenko", "066044231", 19);
        vasyl.setName("Vasyl");
        vasyl.setSurname("Koval");
        vasyl.setPhoneNumber("0987555731");
        vasyl.setAge(23);
        System.out.println("Person Vasyl: " + "Person(name=" + vasyl.getName() + ", surname=" + vasyl.getSurname()
                + ", phoneNumber=" + vasyl.getPhoneNumber() + ", age=" + vasyl.getAge() + ")");
        System.out.println("Person Viktor: " + viktor);

    }
}
