package ua.ithillel.homeworks.homework5;

public class SpecialMachinery extends AbstractCar {
    private final String type;
    private final float weight; // in tons

    public SpecialMachinery(String brand, int year, int price, String equipment, String manufacturerCountry,
                            String dateOfSale, String customerName, CarColors color, String type, float weight) {
        super(brand, year, price, equipment, manufacturerCountry, dateOfSale, customerName, color);
        this.type = type;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Special equipment" + "\n" +
                "Type: " + type + "\n" +
                "Weight: " + weight + " tons" + "\n" +
                super.toString();
    }
}
