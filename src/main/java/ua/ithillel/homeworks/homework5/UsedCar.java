package ua.ithillel.homeworks.homework5;

public class UsedCar extends AbstractCar {
    private final int carMileage; // in km
    private final String ownerName;

    public UsedCar(String brand, int year, int price, String equipment, String manufacturerCountry, String dateOfSale,
                   String customerName, CarColors color, int carMileage, String ownerName) {
        super(brand, year, price, equipment, manufacturerCountry, dateOfSale, customerName, color);
        this.carMileage = carMileage;
        this.ownerName = ownerName;
    }

    @Override
    public String toString() {
        return "Used car" + "\n" +
                "Mileage of car: " + carMileage + "km" + "\n" +
                "Owner full name: " + ownerName + "\n" +
                super.toString();
    }
}
