package ua.ithillel.homeworks.homework5;

public abstract class AbstractCar {
    private final String brand;
    private final int year;
    private final int price; // in dollars
    private final String equipment;
    private final String manufacturerCountry;
    private final String dateOfSale;
    private final String customerName;
    private final CarColors colors;

    public AbstractCar(String brand, int year, int price, String equipment, String manufacturerCountry,
                       String dateOfSale, String customerName, CarColors color) {
        this.brand = brand;
        this.year = year;
        this.price = price;
        this.equipment = equipment;
        this.manufacturerCountry = manufacturerCountry;
        this.dateOfSale = dateOfSale;
        this.customerName = customerName;
        this.colors = color;
    }

    @Override
    public String toString() {
        return "Brand: " + brand + "\n" +
                "Year: " + year + "\n" +
                "Price: " + price + "$" + "\n" +
                "Equipment: " + equipment + "\n" +
                "Made in: " + manufacturerCountry + "\n" +
                "Date of Sale: " + dateOfSale + "\n" +
                "Customer full name: " + customerName + "\n" +
                "Color: " + colors + "\n";
    }
}
