package ua.ithillel.homeworks.homework5;

public enum CarColors {
    BLACK("black"), RED("red"), GREEN("green"), BLUE("blue"), MAGENTA("magenta"), CYAN("cyan"), YELLOW("yellow"),
    GREY("grey");

    private final String color;

    CarColors(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return color;
    }
}
