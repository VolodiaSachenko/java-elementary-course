package ua.ithillel.homeworks.homework5.keeping;

import ua.ithillel.homeworks.homework5.AbstractCar;

public interface CarStorage {

    void addCar(AbstractCar car);

    void showCars();

}
