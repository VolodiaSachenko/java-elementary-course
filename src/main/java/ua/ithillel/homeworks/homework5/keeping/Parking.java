package ua.ithillel.homeworks.homework5.keeping;

import ua.ithillel.homeworks.homework5.AbstractCar;

import java.util.ArrayList;
import java.util.List;

public class Parking implements CarStorage {
    private final List<AbstractCar> carsAtParking = new ArrayList<>();

    @Override
    public void addCar(AbstractCar car) {
        this.carsAtParking.add(car);
    }

    @Override
    public void showCars() {
        for (AbstractCar car : this.carsAtParking) {
            System.out.println("Сar is in the parking lot");
            System.out.println(car);
            System.out.println("* * * * * *" + "\n");
        }
    }
}
