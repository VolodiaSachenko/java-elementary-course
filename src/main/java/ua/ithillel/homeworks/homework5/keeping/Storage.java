package ua.ithillel.homeworks.homework5.keeping;

import ua.ithillel.homeworks.homework5.AbstractCar;

import java.util.ArrayList;
import java.util.List;

public class Storage implements CarStorage {
    private final List<AbstractCar> carsAtStorage = new ArrayList<>();

    @Override
    public void addCar(AbstractCar car) {
        this.carsAtStorage.add(car);
    }

    @Override
    public void showCars() {
        for (AbstractCar car : this.carsAtStorage) {
            System.out.println("Сar is in the storage");
            System.out.println(car);
            System.out.println("* * * * * *" + "\n");
        }
    }
}
