package ua.ithillel.homeworks.homework5;

public class SportCar extends AbstractCar {
    private final int maxSpeed; // in km
    private final float engineCapacity; // in liters
    private final int enginePower; // in horsepower

    public SportCar(String brand, int year, int price, String equipment, String manufacturerCountry, String dateOfSale,
                    String customerName, CarColors color, int maxSpeed, float engineCapacity, int enginePower) {
        super(brand, year, price, equipment, manufacturerCountry, dateOfSale, customerName, color);
        this.maxSpeed = maxSpeed;
        this.engineCapacity = engineCapacity;
        this.enginePower = enginePower;
    }

    @Override
    public String toString() {
        return "Sport car" + "\n" +
                "Max speed: " + maxSpeed + "km" + "\n" +
                "Engine capacity: " + engineCapacity + "l" + "\n" +
                "Engine power: " + enginePower + " horsepower" + "\n" +
                super.toString();
    }
}
