package ua.ithillel.homeworks.homework5;

import ua.ithillel.homeworks.homework5.keeping.Parking;
import ua.ithillel.homeworks.homework5.keeping.Storage;

/**
 * @author Volodia Sachenko
 */

public class Homework5 {
    public static void main(String[] args) {
        Storage storage = new Storage();
        Parking parking = new Parking();

        SpecialMachinery car = new SpecialMachinery("CAT", 2001, 50000, "regular",
                "USA", "12.12.21", "Panchenko Sergij",
                CarColors.YELLOW, "tractor", 5.575f);
        SpecialMachinery car1 = new SpecialMachinery("Belarus", 1987, 4000, "minimal",
                "USSR", "10.09.21", "Petro Schur",
                CarColors.RED, "tractor", 3.5f);
        SportCar car2 = new SportCar("Ferrari", 2012, 180000, "maximal",
                "Italy", "15.08.21", "Ivan Bogach",
                CarColors.CYAN, 310, 4.5f, 570);
        SportCar car3 = new SportCar("Porsche", 2014, 80000, "maximal",
                "Germany", "14.07.20", "Anton Deputatov",
                CarColors.GREY, 300, 3.5f, 480);
        SportCar car4 = new SportCar("Lamborghini", 2021, 400000, "maximal",
                "Italy", "16.10.21", "Victor Banditov",
                CarColors.BLUE, 330, 5.2f, 600);
        UsedCar car5 = new UsedCar("Lada", 1989, 1000, "minimal",
                "USSR", "30.10.21", "Vasyl Semchenko",
                CarColors.MAGENTA, 60000, "Dmytro Dmytriv");
        UsedCar car6 = new UsedCar("ZAZ", 1986, 400, "minimal",
                "USSR", "12.02.21", "Vasyl Filatov",
                CarColors.BLACK, 80000, "Dmytro Rachenko");
        UsedCar car7 = new UsedCar("VAZ", 1988, 3700, "regular",
                "USSR", "20.10.21", "Vasylij Vasylchuk",
                CarColors.GREEN, 50000, "Petro Petrenko");

        storage.addCar(car);
        storage.addCar(car1);
        storage.addCar(car2);
        storage.addCar(car3);

        parking.addCar(car4);
        parking.addCar(car5);
        parking.addCar(car6);
        parking.addCar(car7);

        System.out.println("Cars " + "\uD83D\uDE98 " + " \uD83D\uDE97 " + " \uD83D\uDE9A" + "\n");

        storage.showCars();
        parking.showCars();
    }
}
