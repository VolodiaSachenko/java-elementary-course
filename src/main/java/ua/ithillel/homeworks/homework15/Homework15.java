package ua.ithillel.homeworks.homework15;

import ua.ithillel.homeworks.homework15.container.Container;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author Volodia Sachenko
 */

public class Homework15 {

    public static void main(String[] args) {
        Container containerUSA = new Container(120, "USA");
        Container containerUSA2 = new Container(77, "USA");
        Container containerUkraine = new Container(95, "Ukraine");
        Container containerUkraine2 = new Container(150, "Ukraine");
        Container containerUkraine3 = new Container(80, "Ukraine");
        Container containerJapan = new Container(129, "Japan");

        List<Container> containers = new ArrayList<>(6);
        containers.add(containerUSA);
        containers.add(containerUSA2);
        containers.add(containerUkraine);
        containers.add(containerUkraine2);
        containers.add(containerUkraine3);
        containers.add(containerJapan);

        showContainerInfoConsumer(containerUSA);
        System.out.println("Weight 150 is more than 100: " + checkMoreThanHundredWeightPredicate(containerUkraine2));
        System.out.println("The most popular country is: " + findMostPopularCountryFunction(containers));
    }

    private static void showContainerInfoConsumer(Container info) {
        Consumer<Container> containerWeight = container -> System.out.println("Container with weight " +
                container.getWeight() + " was boarded");
        Consumer<Container> containerCountry = container -> System.out.println("Container goes to " +
                container.getCountry());
        containerWeight.andThen(containerCountry).accept(info);
    }

    private static boolean checkMoreThanHundredWeightPredicate(Container container) {
        Predicate<Container> checkWeight = containerWeight -> containerWeight.getWeight() > 100;
        return checkWeight.test(container);
    }

    private static String findMostPopularCountryFunction(List<Container> containers) {
        Function<List<Container>, String> findMostPopularCountry = findContainer -> {
            Map<String, Integer> matches = new HashMap<>();
            for (Container container : containers) {
                if (!matches.containsKey(container.getCountry())) {
                    matches.put(container.getCountry(), 1);
                } else {
                    matches.put(container.getCountry(), matches.get(container.getCountry()) + 1);
                }
            }
            return Collections.max(matches.keySet());
        };
        return findMostPopularCountry.apply(containers);
    }
}
