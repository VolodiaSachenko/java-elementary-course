package ua.ithillel.homeworks.homework15.container;

public class Container {

    private final int weight;
    private final String country;

    public Container(int weight, String country) {
        this.weight = weight;
        this.country = country;
    }

    public int getWeight() {
        return weight;
    }

    public String getCountry() {
        return country;
    }
}
