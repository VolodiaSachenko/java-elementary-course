package ua.ithillel.homeworks.homework19.mapper;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import ua.ithillel.homeworks.homework19.RunnableProcess;
import ua.ithillel.homeworks.homework19.model.MachineReport;
import ua.ithillel.homeworks.homework19.model.Status;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.util.stream.Collectors.groupingBy;

public class OpenCSVMapper implements ReportMapper<MachineReport> {

    private static final String CSV_PATH = "src/main/resources/machine-readable.csv";

    private Map<Status, List<MachineReport>> collectMapByStatus(List<MachineReport> reportList) {
        return reportList.stream().collect(groupingBy(MachineReport::getStatus));
    }

    private static void printInfo(Map<Status, List<MachineReport>> map, long elapsedTime, String message) {
        System.out.println("Starting " + message + " execution");
        map.forEach((key, value) -> System.out.println("Status " + key + " : " + value.size() + " reports"));
        System.out.println("Execution time : " + elapsedTime + "ms");
    }

    private void executeParallel(Map<Status, List<MachineReport>> map,
                                 List<MachineReport> reportList) {
        List<MachineReport> subList = reportList.subList(0, 6346);
        List<MachineReport> subList1 = reportList.subList(6346, 12693);
        List<MachineReport> subList2 = reportList.subList(12693, 19093);

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        RunnableProcess runnableProcess = new RunnableProcess(map, subList);
        RunnableProcess runnableProcess2 = new RunnableProcess(map, subList1);
        RunnableProcess runnableProcess3 = new RunnableProcess(map, subList2);

        Future<?> task1 = executorService.submit(runnableProcess);
        Future<?> task2 = executorService.submit(runnableProcess2);
        Future<?> task3 = executorService.submit(runnableProcess3);
        try {
            task1.get();
            task2.get();
            task3.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }

    @Override
    public List<MachineReport> readAll(List<String[]> lines) {
        List<MachineReport> machineReports = new ArrayList<>();
        for (int i = 1; i < lines.size(); i++) {
            String[] line = lines.get(i);
            MachineReport machineReport = new MachineReport();
            machineReport.setSeriesReference(line[0]);
            try {
                machineReport.setPeriod(new SimpleDateFormat("yyyy.MM").parse(line[1]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            machineReport.setDataValue(line[2]);
            machineReport.setSuppressed(MachineReport.parseBoolean(line[3]));
            machineReport.setStatus(Status.valueOf(line[4]));
            machineReport.setUnits(line[5]);
            machineReport.setMagnitude(line[6]);
            machineReport.setSubject(line[7]);
            machineReport.setGroup(line[8]);
            machineReport.setSeriesTitle1(line[9]);
            machineReport.setSeriesTitle2(line[10]);
            machineReport.setSeriesTitle3(line[11]);
            machineReport.setSeriesTitle4(line[12]);
            machineReport.setSeriesTitle5(line[13]);
            machineReports.add(machineReport);
        }
        return machineReports;
    }

    @Override
    public void populateMap(Map<Status, List<MachineReport>> map, List<MachineReport> reportList) {
        long startTime = System.currentTimeMillis();
        map = collectMapByStatus(reportList);
        long elapsedTime = System.currentTimeMillis() - startTime;
        printInfo(map, elapsedTime, "non-parallel");
    }

    @Override
    public void populateMapParallel(Map<Status, List<MachineReport>> map, List<MachineReport> reportList)
            throws InterruptedException {
        long startTime = System.currentTimeMillis();
        map.put(Status.C, new ArrayList<>());
        map.put(Status.R, new ArrayList<>());
        map.put(Status.F, new ArrayList<>());
        executeParallel(map, reportList);
        long elapsedTime = System.currentTimeMillis() - startTime;
        printInfo(map, elapsedTime, "parallel");
    }

    public static List<String[]> readAll() throws URISyntaxException, IOException, CsvException {
        CSVReader csvReader = new CSVReader(new FileReader(CSV_PATH));
        return csvReader.readAll();
    }
}

