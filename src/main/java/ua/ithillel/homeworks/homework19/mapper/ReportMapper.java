package ua.ithillel.homeworks.homework19.mapper;

import ua.ithillel.homeworks.homework19.model.Status;
import ua.ithillel.homeworks.homework19.model.MachineReport;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public interface ReportMapper<T> {

    List<T> readAll(List<String[]> lines);

    void populateMap(Map<Status, List<MachineReport>> map, List<MachineReport> reportList);

    void populateMapParallel(Map<Status, List<MachineReport>> map, List<MachineReport> reportList)
            throws InterruptedException, ExecutionException;
}
