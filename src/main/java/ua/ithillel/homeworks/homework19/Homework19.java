package ua.ithillel.homeworks.homework19;

import com.opencsv.exceptions.CsvException;
import ua.ithillel.homeworks.homework19.mapper.OpenCSVMapper;
import ua.ithillel.homeworks.homework19.model.MachineReport;
import ua.ithillel.homeworks.homework19.model.Status;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Volodia Sachenko
 */

public class Homework19 {
    public static void main(String[] args) {
        try {
            List<String[]> readAll = OpenCSVMapper.readAll();
            OpenCSVMapper openCSVParser = new OpenCSVMapper();
            List<MachineReport> machineReports = openCSVParser.readAll(readAll);
            machineReports.forEach(System.out::println);

            Map<Status, List<MachineReport>> map = new HashMap<>();
            openCSVParser.populateMap(map, machineReports);

            System.out.println("===========================");

            Map<Status, List<MachineReport>> map2 = new ConcurrentHashMap<>();
            openCSVParser.populateMapParallel(map2, machineReports);
        } catch (URISyntaxException | IOException | CsvException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
