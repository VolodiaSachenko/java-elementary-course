package ua.ithillel.homeworks.homework19.model;

import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Data;
import ua.ithillel.homeworks.homework19.adapter.DateAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class MachineReport {
    private String seriesReference;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date period;
    private String dataValue;
    private boolean suppressed;
    private Status status;
    private String units;
    private String magnitude;
    private String subject;
    private String group;
    private String seriesTitle1;
    private String seriesTitle2;
    private String seriesTitle3;
    private String seriesTitle4;
    private String seriesTitle5;

    public static DateFormat simpleDateFormat() {
        return new SimpleDateFormat("yyyy.MM");
    }

    public static boolean parseBoolean(String value) {
        return value.equals("Y");
    }

    @Override
    public String toString() {
        return "MachineReport (" +
                "seriesReference='" + seriesReference + '\'' +
                ", period=" + simpleDateFormat().format(period) +
                ", dataValue='" + dataValue + '\'' +
                ", suppressed=" + suppressed +
                ", status=" + status +
                ", units='" + units + '\'' +
                ", magnitude='" + magnitude + '\'' +
                ", subject='" + subject + '\'' +
                ", group='" + group + '\'' +
                ", seriesTitle1='" + seriesTitle1 + '\'' +
                ", seriesTitle2='" + seriesTitle2 + '\'' +
                ", seriesTitle3='" + seriesTitle3 + '\'' +
                ", seriesTitle4='" + seriesTitle4 + '\'' +
                ", seriesTitle5='" + seriesTitle5 + '\'' +
                ')' + "\n";
    }
}
