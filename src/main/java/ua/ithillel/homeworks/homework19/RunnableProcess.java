package ua.ithillel.homeworks.homework19;

import ua.ithillel.homeworks.homework19.model.MachineReport;
import ua.ithillel.homeworks.homework19.model.Status;

import java.util.List;
import java.util.Map;

public class RunnableProcess implements Runnable {

    private final Map<Status, List<MachineReport>> map;
    private final List<MachineReport> list;

    public RunnableProcess(Map<Status, List<MachineReport>> map, List<MachineReport> list) {
        this.map = map;
        this.list = list;
    }

    @Override
    public void run() {
        synchronized (map) {
            for (MachineReport report : list) {
                switch (report.getStatus()) {
                    case C -> map.get(Status.C).add(report);
                    case F -> map.get(Status.F).add(report);
                    case R -> map.get(Status.R).add(report);
                }
            }
        }
    }
}
