package ua.ithillel.homeworks.homework23.mapper;

import ua.ithillel.homeworks.homework23.domain.Order;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderMapper implements DatabaseObjectMapper<Order> {
    @Override
    public Order mapObject(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            return mapOrder(resultSet);
        }
        return new Order();
    }

    @Override
    public List<Order> mapObjects(ResultSet resultSet) throws SQLException {
        List<Order> orders = new ArrayList<>();
        while (resultSet.next()) {
            orders.add(mapOrder(resultSet));
        }
        return orders;
    }

    private Order mapOrder(ResultSet resultSet) throws SQLException {
        Order order = new Order();
        order.setId(resultSet.getInt("id"));
        order.setDate(resultSet.getTimestamp("date"));
        order.setCarId(resultSet.getInt("car_id"));
        order.setClientId(resultSet.getInt("client_id"));
        order.setManagerId(resultSet.getInt("manager_id"));
        return order;
    }
}
