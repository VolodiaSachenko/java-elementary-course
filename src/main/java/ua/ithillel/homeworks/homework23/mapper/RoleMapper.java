package ua.ithillel.homeworks.homework23.mapper;

import ua.ithillel.homeworks.homework23.domain.Role;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleMapper implements DatabaseObjectMapper<Role> {
    @Override
    public Role mapObject(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            return mapRole(resultSet);
        } else {
            return null;
        }
    }

    @Override
    public List<Role> mapObjects(ResultSet resultSet) throws SQLException {
        List<Role> roles = new ArrayList<>();
        while (resultSet.next()) {
            roles.add(mapRole(resultSet));
        }
        return roles;
    }

    private Role mapRole(ResultSet resultSet) throws SQLException {
        return Role.valueOfId(resultSet.getInt("id"));
    }
}
