package ua.ithillel.homeworks.homework23;

import ua.ithillel.homeworks.homework23.connection.DatabaseConfig;
import ua.ithillel.homeworks.homework23.domain.Car;
import ua.ithillel.homeworks.homework23.domain.Manager;
import ua.ithillel.homeworks.homework23.domain.Order;
import ua.ithillel.homeworks.homework23.domain.Role;
import ua.ithillel.homeworks.homework23.mapper.CarMapper;
import ua.ithillel.homeworks.homework23.mapper.DatabaseObjectMapper;
import ua.ithillel.homeworks.homework23.mapper.ManagerMapper;
import ua.ithillel.homeworks.homework23.mapper.OrderMapper;
import ua.ithillel.homeworks.homework23.mapper.RoleMapper;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author Volodia Sachenko
 */

public class Homework23 {
    public static void main(String[] args) {
        DatabaseConfig config = new DatabaseConfig();
        try (Connection connection = config.getConnection();
             Statement statement = connection.createStatement()) {

            DatabaseObjectMapper<Car> carMapper = new CarMapper();
            DatabaseObjectMapper<Order> orderMapper = new OrderMapper();
            DatabaseObjectMapper<Manager> managerMapper = new ManagerMapper();
            DatabaseObjectMapper<Role> roleMapper = new RoleMapper();

            System.out.println("Roles:");
            String rolesTable = "SELECT * FROM car_rent.roles";
            ResultSet roles = statement.executeQuery(rolesTable);
            List<Role> rolesList = roleMapper.mapObjects(roles);
            rolesList.forEach(System.out::println);

            System.out.println("\n1) Получить всех менеджеров у которых роль «Админ»:");
            String query1 = "SELECT * FROM car_rent.managers WHERE role = 1";
            ResultSet resultSet1 = statement.executeQuery(query1);
            List<Manager> managerList = managerMapper.mapObjects(resultSet1);
            managerList.forEach(System.out::println);
            resultSet1.close();

            System.out.println("\n2) Получить все заказы у которых дата исполнения позже 01.28.2022:");
            String query2 = "SELECT * FROM car_rent.orders WHERE date > '2022-01-28'";
            ResultSet resultSet2 = statement.executeQuery(query2);
            List<Order> ordersDate = orderMapper.mapObjects(resultSet2);
            ordersDate.forEach(System.out::println);
            resultSet2.close();

            System.out.println("\n3) Получить автомобиль у которого самая высокая цена:");
            String query3 = "SELECT * FROM car_rent.cars WHERE price = (SELECT MAX(price) FROM car_rent.cars)";
            ResultSet resultSet3 = statement.executeQuery(query3);
            Car highestPrice = carMapper.mapObject(resultSet3);
            System.out.println(highestPrice);
            resultSet3.close();

            String query4 = "SELECT COUNT(role) FROM car_rent.managers WHERE role = 2";
            ResultSet resultSet4 = statement.executeQuery(query4);
            System.out.print("\n4) Получить количество менеджеров у которых роль “Manager”:\nManagers count: ");
            while (resultSet4.next()) {
                System.out.print(resultSet4.getInt(1) + "\n");
            }
            resultSet4.close();

            System.out.println("\n5) Получить изготовителя и марку всех автомобилей у которых есть заказы в таблице orders:");
            System.out.println("----------------------");
            System.out.println("manufacturer     model");
            System.out.println("----------------------");
            String query5 = "SELECT manufacturer, model FROM car_rent.cars INNER JOIN car_rent.orders ON cars.id = orders.car_id";
            ResultSet resultSet5 = statement.executeQuery(query5);
            while (resultSet5.next()) {
                System.out.println(resultSet5.getString(1) + "  " + resultSet5.getString(2));
            }
            resultSet5.close();

            System.out.println("\n6) Получить количество изготовителей автомобилей в таблице cars:");
            System.out.println("----------------------");
            System.out.println("manufacturer     count");
            System.out.println("----------------------");
            String query6 = "SELECT manufacturer, COUNT(manufacturer) FROM car_rent.cars GROUP BY manufacturer";
            ResultSet resultSet6 = statement.executeQuery(query6);
            while (resultSet6.next()) {
                System.out.println(resultSet6.getString(1) + ": " + resultSet6.getString(2));
            }
            resultSet6.close();

            System.out.println("7) Написать транзакцию которая будет добавлять два новых заказа в таблицу " +
                    "car_rent.orders и изменять поля\n" + "available на false у автомобилей которые указаны в " +
                    "ранее добавленных заказах:");
            String insert = "INSERT INTO car_rent.orders (id, date, car_id, client_id, manager_id) VALUES (?, ?, ?, ?, ?)";
            String update = "UPDATE car_rent.cars SET available = FALSE WHERE id = ?";
            PreparedStatement insertValue = connection.prepareStatement(insert);
            PreparedStatement updateCars = connection.prepareStatement(update);

            connection.setAutoCommit(false);
            insertValue.setInt(1, 9);
            insertValue.setDate(2, new Date(2022 - 6 - 10));
            insertValue.setInt(3, 10);
            insertValue.setInt(4, 2);
            insertValue.setInt(5, 2);
            insertValue.executeUpdate();

            insertValue.setInt(1, 10);
            insertValue.setDate(2, new Date(2022 - 6 - 11));
            insertValue.setInt(3, 11);
            insertValue.setInt(4, 3);
            insertValue.setInt(5, 3);
            insertValue.executeUpdate();

            updateCars.setInt(1, 10);
            updateCars.executeUpdate();

            updateCars.setInt(1, 11);
            updateCars.executeUpdate();

            connection.commit();
            insertValue.close();
            updateCars.close();

            System.out.println("\nSELECT * FROM car_rent.orders ORDER BY id:");
            String showOrders = "SELECT * FROM car_rent.orders ORDER BY id";
            ResultSet orderSet = statement.executeQuery(showOrders);
            List<Order> orders = orderMapper.mapObjects(orderSet);
            orders.forEach(System.out::println);
            orderSet.close();

            String showCars = "SELECT * FROM car_rent.cars ORDER BY id";
            ResultSet carSet = statement.executeQuery(showCars);
            List<Car> cars = carMapper.mapObjects(carSet);
            System.out.println("\nSELECT * FROM car_rent.cars ORDER BY id:");
            cars.forEach(System.out::println);
            carSet.close();

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
