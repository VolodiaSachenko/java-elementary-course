package ua.ithillel.homeworks.homework23.domain;

import java.util.Arrays;

public enum Role {
    ADMIN(1), MANAGER(2), CLIENT(3);
    private final int id;

    Role(int id) {
        this.id = id;
    }

    public static Role valueOfId(int idValue) {
        return Arrays.stream(Role.values())
                .filter(role -> role.id == idValue).findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("There is no id: %s.", idValue)));
    }

    @Override
    public String toString() {
        if (id == 1) {
            return "Admin";
        }
        if (id == 2) {
            return "Manager";
        }
        if (id == 3) {
            return "Client";
        } else {
            return "Role is null";
        }
    }
}
