package ua.ithillel.homeworks.homework23.domain;

import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
public class Car {
    private int id;
    private String manufacturer;
    private String model;
    private int year;
    private int price;
    private boolean available;
}