package ua.ithillel.homeworks.homework23.domain;

import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Setter
@ToString
public class Order {
    private int id;
    private Date date;
    private int carId;
    private int clientId;
    private int managerId;
}
