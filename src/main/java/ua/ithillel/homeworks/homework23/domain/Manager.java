package ua.ithillel.homeworks.homework23.domain;

import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
public class Manager {
    private int id;
    private String name;
    private String login;
    private String password;
    private String email;
    private Role role;
}
