package ua.ithillel.homeworks.homework16;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author Volodia Sachenko
 */

public class Homework16 {
    public static void main(String[] args) {
        System.out.println("100 random letters:");
        List<String> randomLetters = randomLetters();
        randomLetters.forEach(System.out::print);

        System.out.println("\n" + "The most popular letter(s):");
        findMostPopularLetter(randomLetters);

        System.out.println("Sorted letters:");
        sortLetters(randomLetters);

        System.out.println("\n" + "Reversed order:");
        sortLettersReverse(randomLetters);
    }

    private static List<String> randomLetters() {
        return Arrays.stream(new Random()
                        .ints(100, 97, 123)
                        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                        .toString()
                        .split(""))
                .collect(Collectors.toList());
    }

    private static void findMostPopularLetter(List<String> letterList) {
        Map<String, Long> mostPopularLetter = letterList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .max(Map.Entry.comparingByValue()).stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
        mostPopularLetter.forEach((key, value) ->
                System.out.println(("Letter " + "\"" + key + "\"" + " - " + value + " times")));
    }

    private static void sortLetters(List<String> letterList) {
        List<String> sortedLetters = letterList
                .stream()
                .sorted()
                .toList();
        sortedLetters.forEach(System.out::print);
    }

    private static void sortLettersReverse(List<String> letterList) {
        List<String> sortedLetters = letterList
                .stream()
                .sorted(Comparator.reverseOrder())
                .toList();
        sortedLetters.forEach(System.out::print);
    }
}
