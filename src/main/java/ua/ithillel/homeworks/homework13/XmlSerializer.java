package ua.ithillel.homeworks.homework13;

import ua.ithillel.homeworks.homework13.annotation.XmlElement;
import ua.ithillel.homeworks.homework13.annotation.XmlSerializable;
import ua.ithillel.homeworks.homework13.exception.XmlSerializableException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class XmlSerializer {

    public <T> List<String> serialize(List<T> objects) throws XmlSerializableException, NullPointerException,
            IllegalAccessException {
        if (objects == null) {
            throw new NullPointerException("The object is null");
        }
        List<String> serialized = new ArrayList<>();
        for (Object object : objects) {
            Class<?> clazz = object.getClass();
            if (clazz.isAnnotationPresent(XmlSerializable.class)) {
                StringBuilder value = new StringBuilder();
                if (!clazz.getAnnotation(XmlSerializable.class).key().equals("")) {
                    value.append("<").append(clazz.getAnnotation(XmlSerializable.class).key()).append(">").append("\n");
                } else {
                    value.append("<").append(clazz.getSimpleName()).append(">").append("\n");
                }
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(XmlElement.class)) {
                        if (!field.getAnnotation(XmlElement.class).key().equals("")) {
                            value.append("  ").append("<").append(field.getAnnotation(XmlElement.class).key())
                                    .append(">")
                                    .append(field.get(object))
                                    .append("</")
                                    .append(field.getAnnotation(XmlElement.class).key())
                                    .append(">")
                                    .append("\n");
                        } else {
                            value.append("  ").append("<").append(field.getName()).append(">")
                                    .append(field.get(object))
                                    .append("</")
                                    .append(field.getName())
                                    .append(">")
                                    .append("\n");
                        }
                    }
                }
                if (!clazz.getAnnotation(XmlSerializable.class).key().equals("")) {
                    value.append("</").append(clazz.getAnnotation(XmlSerializable.class).key()).append(">");
                } else {
                    value.append("</").append(clazz.getSimpleName()).append(">");
                }
                serialized.add(value.toString());
            } else {
                throw new
                        XmlSerializableException("The class with name <" + clazz.getSimpleName() +
                        "> is not annotated with @XmlSerializable");
            }
        }
        return serialized;
    }
}
