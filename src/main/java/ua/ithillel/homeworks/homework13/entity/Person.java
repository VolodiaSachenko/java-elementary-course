package ua.ithillel.homeworks.homework13.entity;

import ua.ithillel.homeworks.homework13.annotation.XmlElement;
import ua.ithillel.homeworks.homework13.annotation.XmlSerializable;

@XmlSerializable(key = "Person")
public class Person {

    @XmlElement(key = "firstName")
    private String name;
    @XmlElement(key = "lastName")
    private String surname;
    @XmlElement
    private String address;

    public Person(String name, String surname, String address) {
        this.name = name;
        this.surname = surname;
        this.address = address;
    }
}
