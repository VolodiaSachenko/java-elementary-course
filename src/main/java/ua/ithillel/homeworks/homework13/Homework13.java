package ua.ithillel.homeworks.homework13;

import ua.ithillel.homeworks.homework13.entity.Person;
import ua.ithillel.homeworks.homework13.exception.XmlSerializableException;

import java.util.List;

/**
 * @author Volodia Sachenko
 */

public class Homework13 {

    public static void main(String[] args) {

        XmlSerializer personXmlSerializer = new XmlSerializer();

        Person person = new Person("Ivan", "Ivanov", "Kyiv");
        Person person2 = new Person("Oleg", "Petrenko", "Kyiv");

        try {
            List<String> stringList = personXmlSerializer.serialize(List.of(person, person2));
            stringList.forEach(System.out::println);
        } catch (NullPointerException | XmlSerializableException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
