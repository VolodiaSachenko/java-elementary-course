package ua.ithillel.homeworks.homework3;

import java.util.List;

public class Student extends SchoolMemberWithGroups {
    private final short rating;
    private final short payment;
    private final boolean covidVaccine;

    public Student(byte age, String phone, String firstName, String secondName, String email, List<Group> groups,
                   short rating, short payment, boolean covidVaccine) {
        super(age, phone, firstName, secondName, email, groups);
        this.rating = rating;
        this.payment = payment;
        this.covidVaccine = covidVaccine;
    }

    @Override
    public String toString() {
        String result;
        if (covidVaccine) {
            result = "yes";
        } else {
            result = "no";
        }
        return "Student " + super.toString() + "\n" +
                "Rating : " + rating + "\n" +
                "Payment : " + payment + " grn" + "\n" +
                "Covid vaccine : " + result + "\n" +
                "Groups : " + "\n" + getGroups() + "\n";
    }
}
