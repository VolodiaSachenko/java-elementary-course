package ua.ithillel.homeworks.homework3;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Volodia Sachenko
 */

public class Homework3 {
    public static void main(String[] args) {
        List<Group> groups = new ArrayList<>(3);
        groups.add(new Group("Java Elementary (PROGRAMMING)", "01.10.21", "01.12.21"));
        groups.add(new Group("QA (TESTING)", "02.11.21", "02.12.21"));
        groups.add(new Group("Recruiting & HR (MANAGEMENT)", "01.08.21", "01.10.21"));
        groups.add(new Group("Python Elementary (PROGRAMMING)", "01.09.21", "01.11.21"));

        List<SchoolMemberWithGroups> schoolMemberWithGroups = new ArrayList<>();
        schoolMemberWithGroups.add(new Student((byte) 22, "+380973855932", "Ivan",
                "Harchenko", "harchenko@gmail.com", List.of(groups.get(0), groups.get(1)),
                (short) 1450, (short) 10000, true));
        schoolMemberWithGroups.add(new Student((byte) 28, "+380953855932", "Oleg",
                "Olegiv", "olegiv@gmail.com", List.of(groups.get(2)),
                (short) 1345, (short) 10000, true));
        schoolMemberWithGroups.add(new Student((byte) 35, "+380683877932", "Vasyl",
                "Vasylciv", "vasylciv@gmail.com", List.of(groups.get(3)),
                (short) 1600, (short) 15000, false));

        schoolMemberWithGroups.add(new Teacher((byte) 30, "+380501543423", "Stepan",
                "Kachan", "teacher@gmail.com", List.of(groups.get(0)),
                (byte) 10, "Infopulse", "Java & Python developer"));
        schoolMemberWithGroups.add(new Teacher((byte) 32, "+380661855834", "Sergei",
                "Klunyy", "teacherhillel@gmail.com", List.of(groups.get(0), groups.get(2)),
                (byte) 7, "Playtech", "Java developer"));
        schoolMemberWithGroups.add(new Teacher((byte) 45, "+380930211245", "Igor",
                "Shklar", "shklar@gmail.com", List.of(groups.get(0), groups.get(3)),
                (byte) 12, "Alfa Bank", "Java developer"));

        SchoolMemberWithGroups schoolMemberWithGroups1 = new SchoolMemberWithGroups();
        schoolMemberWithGroups1.show(schoolMemberWithGroups);
    }
}
