package ua.ithillel.homeworks.homework3;

import java.util.List;

public class Teacher extends SchoolMemberWithGroups {
    private final byte experience;
    private final String job;
    private final String mainSkill;

    public Teacher(byte age, String phone, String firstName, String secondName, String email, List<Group> groups,
                   byte experience, String job, String mainSkill) {
        super(age, phone, firstName, secondName, email, groups);
        this.experience = experience;
        this.job = job;
        this.mainSkill = mainSkill;
    }

    @Override
    public String toString() {
        return "Teacher " + super.toString() + "\n" +
                "Experience : " + experience + " years" + "\n" +
                "Company : " + job + "\n" +
                "Professional skill : " + mainSkill + "\n" +
                "Groups : " + "\n" + getGroups() + "\n";
    }
}
