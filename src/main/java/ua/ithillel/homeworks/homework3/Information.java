package ua.ithillel.homeworks.homework3;

import java.util.List;

public interface Information {

    void show(List<SchoolMemberWithGroups> members);

}
