package ua.ithillel.homeworks.homework3;

import java.util.List;

public class SchoolMemberWithGroups extends SchoolMembers implements Information {
    private List<Group> groups;

    public SchoolMemberWithGroups() {
    }

    public SchoolMemberWithGroups(byte age, String phone, String firstName, String secondName, String email,
                                  List<Group> groups) {
        super(age, phone, firstName, secondName, email);
        this.groups = groups;
    }

    @Override
    public void show(List<SchoolMemberWithGroups> members) {
        for (SchoolMemberWithGroups member : members) {
            System.out.println(member);
            System.out.println("""
                    -------------
                    """);
        }
    }

    public List<Group> getGroups() {
        return groups;
    }
}
