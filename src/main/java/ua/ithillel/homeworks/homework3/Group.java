package ua.ithillel.homeworks.homework3;

public class Group {
    private final String group;
    private final String startDate;
    private final String endDate;

    public Group(String group, String startDate, String endDate) {
        this.group = group;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return group + " Start: " + startDate + " End: " + endDate;
    }
}
