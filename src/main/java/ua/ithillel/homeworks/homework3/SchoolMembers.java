package ua.ithillel.homeworks.homework3;

public abstract class SchoolMembers {
    private byte age;
    private String phone;
    private String firstName;
    private String secondName;
    private String email;

    public SchoolMembers() {
    }

    public SchoolMembers(byte age, String phone, String firstName, String secondName, String email) {
        this.age = age;
        this.phone = phone;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
    }

    @Override
    public String toString() {
        return firstName + " " + "\n" +
                "Second name : " + secondName + "\n" +
                "Age : " + age + "\n" +
                "Phone number : " + phone + "\n" +
                "Email : " + email;
    }
}
