package ua.ithillel.homeworks.homework10;

public class User {

    private final int id;
    private final String userName;
    private final String email;
    private final String password;
    private final Role role;

    public User(int id, String userName, String email, String password, Role role) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "user: " + userName + "\n" +
                "id: " + id + "\n" +
                "email: " + email + "\n" +
                "password: " + password + "\n" +
                "role: " + role + "\n";
    }
}
