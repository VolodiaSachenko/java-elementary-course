package ua.ithillel.homeworks.homework10;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String message) {
        super(message);
    }

    public static void checkUser(String checkName) throws UserNotFoundException {
        if (checkName == null) {
            throw new UserNotFoundException("src/main/resources/users.csv");
        }
    }
}
