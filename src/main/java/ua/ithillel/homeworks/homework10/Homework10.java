package ua.ithillel.homeworks.homework10;

import java.util.List;

/**
 * @author Volodia Sachenko
 */

public class Homework10 {
    public static void main(String[] args) {

        FileDataMapper fileDataMapper = new FileDataMapper();

        User user1 = fileDataMapper.findUserById(6);
        System.out.println("findUserById(6): " + "\n" + user1);

        User user2 = fileDataMapper.findUserByUsername("Ivan Ivanov");
        System.out.println("findUserByUsername(\"Ivan Ivanov\"): " + "\n" + user2);

        User user3 = fileDataMapper.findUserByEmail("kristiecole@quadeebo.com");
        System.out.println("findUserByEmail(\"kristiecole@quadeebo.com\"): " + "\n" + user3);

        List<Integer> idList = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        List<User> users = fileDataMapper.findUsersByIds(idList);
        System.out.println("findUsersByIds(idList):" + "\n" + users);

        List<User> usersRolesSeo = fileDataMapper.findUsersByRole(Role.CEO);
        System.out.println("\n" + "findUsersByRole(Role.CEO): " + "\n" + usersRolesSeo);

        List<User> usersRolesQa = fileDataMapper.findUsersByRole(Role.QA);
        System.out.println("\n" + "findUsersByRole(Role.QA): " + "\n" + usersRolesQa);

        List<User> usersRolesManager = fileDataMapper.findUsersByRole(Role.MANAGER);
        System.out.println("\n" + "findUsersByRole(Role.MANAGER): " + "\n" + usersRolesManager);

        List<User> usersRolesDeveloper = fileDataMapper.findUsersByRole(Role.DEVELOPER);
        System.out.println("\n" + "findUsersByRole(Role.DEVELOPER): " + "\n" + usersRolesDeveloper);
    }
}
