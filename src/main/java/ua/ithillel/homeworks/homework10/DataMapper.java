package ua.ithillel.homeworks.homework10;

import java.util.List;

public interface DataMapper {

    User findUserById(int id);

    User findUserByUsername(String userName);

    User findUserByEmail(String email);

    List<User> findUsersByIds(List<Integer> ids);
}
