package ua.ithillel.homeworks.homework10;

public enum Role {

    CEO("CEO"), DEVELOPER("Developer"), MANAGER("Manager"), QA("QA");
    private final String role;

    Role(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}
