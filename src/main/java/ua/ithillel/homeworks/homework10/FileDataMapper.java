package ua.ithillel.homeworks.homework10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileDataMapper implements DataMapper {

    private final static String FILE_PATH = "src/main/resources/users.csv";

    @Override
    public User findUserById(int id) {
        return findUser(Integer.toString(id), 0);
    }

    @Override
    public User findUserByUsername(String userName) {
        return findUser(userName, 1);
    }

    @Override
    public User findUserByEmail(String email) {
        return findUser(email, 2);
    }

    @Override
    public List<User> findUsersByIds(List<Integer> id) {
        List<User> users = new ArrayList<>(id.size());
        for (Integer i : id) {
            users.add(findUserById(i));
        }
        return users;
    }

    public List<User> findUsersByRole(Role role) {
        List<User> users = new ArrayList<>();
        List<String> text = parseText();
        for (String line : text) {
            String[] splitColumns = line.split("\\|");
            if (splitColumns[4].equals(role.toString())) {
                users.add(createNewUser(splitColumns));
            }
        }
        return users;
    }

    private List<String> parseText() {
        List<String> text = new ArrayList<>();
        try {
            text = Files.readAllLines(Path.of(FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        return text;
    }

    private User createNewUser(String[] splitColumns) {
        return new User((Integer.parseInt(splitColumns[0])),
                splitColumns[1],
                splitColumns[2],
                splitColumns[3],
                Role.valueOf(splitColumns[4].toUpperCase()));
    }

    private User findUser(String checkValue, int columnIndex) {
        User user = new User(0, null, null, null, null);
        List<String> text = parseText();
        try {
            for (String line : text) {
                String[] splitColumns = line.split("\\|");
                if (splitColumns[columnIndex].equals(checkValue)) {
                    user = createNewUser(splitColumns);
                    return user;
                }
            }
            UserNotFoundException.checkUser(user.getUserName());
        } catch (UserNotFoundException e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
        return user;
    }
}
