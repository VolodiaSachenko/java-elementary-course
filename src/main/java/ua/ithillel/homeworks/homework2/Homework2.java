package ua.ithillel.homeworks.homework2;

import java.util.Arrays;

/**
 * @author Volodia Sachenko
 */

public class Homework2 {
    // Задача №1
    private static void showHundred() {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print("HelloWorld ");
            } else if (i % 3 == 0) {
                System.out.print("Hello ");
            } else if (i % 5 == 0) {
                System.out.print("World ");
            } else {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    //Задача №2
    private static byte[] randomFill(byte[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (byte) (10 + (Math.random() * 11));
        }
        return array;
    }

    private static void findAverage(byte[] array) {
        double average = 0;
        if (array.length > 0) {
            double sum = 0;
            for (byte b : array) {
                sum += b;
            }
            average += sum / array.length;
        } else {
            System.out.println("Помилка, мінімальний розмір масива: 1");
        }
        byte result = (byte) Math.round(average);
        System.out.println("Середнє арифметичне масиву: " + result);
    }

    // Задача №3
    private static int[] sumTarget(int[] nums, int target) {
        int[] index = {-1, -1};

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    index[0] = i;
                    index[1] = j;
                }
            }
        }
        return index;
    }

    public static void main(String[] args) {
        // Задача №2
        byte[] array = new byte[10];
        byte[] randomArray = randomFill(array);
        // Задача №3
        int[] nums = {2, 7, 11, 15};
        int[] nums2 = {3, 2, 4};
        int[] nums3 = {4, 7, 4, 5, 6};
        int[] targets = sumTarget(nums, 9);
        int[] targets2 = sumTarget(nums2, 6);
        int[] targets3 = sumTarget(nums3, 3);
        // Задача №1
        System.out.println("Задача №1:");
        showHundred();
        // Задача №2
        System.out.println("\nЗадача №2:\nМасив byte[] array = " + Arrays.toString(randomArray));
        findAverage(randomArray);
        //Задача №3
        System.out.println("\nЗадача №3:\nInput: int[] nums = " + Arrays.toString(nums));
        System.out.println("Input: int[] nums2 = " + Arrays.toString(nums2));
        System.out.println("Input: int[] nums3 = " + Arrays.toString(nums3));
        System.out.println("int[] nums, target = 9 Output: " + Arrays.toString(targets));
        System.out.println("int[] nums2, target = 6 Output: " + Arrays.toString(targets2));
        System.out.println("int[] nums3, target = 3 Output: " + Arrays.toString(targets3));
    }
}
