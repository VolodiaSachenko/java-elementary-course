package ua.ithillel.homeworks.homework7;

public interface IntStack {
    int pop(); // remove and get value on top of Stack

    int peek(); // get value on top of Stack
}
