package ua.ithillel.homeworks.homework7;

import java.util.Arrays;

public class Homework7 {
    public static void main(String[] args) {
        IntLinkedList intLinkedList = new IntLinkedList();
        System.out.println("IntLinkedList is empty: " + intLinkedList);
        intLinkedList.add(0);
        intLinkedList.add(1);
        intLinkedList.add(2);
        intLinkedList.add(3);
        intLinkedList.add(4);
        intLinkedList.add(5);
        intLinkedList.add(6);
        System.out.println("void add(int value): " + intLinkedList + "\n");

        boolean test = intLinkedList.add(7, 89);
        System.out.println("boolean add(7, 89): " + test);
        System.out.println(intLinkedList + "\n");

        boolean test2 = intLinkedList.isEmpty();
        System.out.println("isEmpty(): " + test2 + "\n");

        boolean removeIndex = intLinkedList.remove(7);
        System.out.println("remove(7): " + removeIndex);
        System.out.println(intLinkedList + "\n");

        int get = intLinkedList.get(6);
        System.out.println("int get(6): " + get + "\n");

        boolean value = intLinkedList.removeByValue(0);
        System.out.println("removeByValue(0): " + intLinkedList + " " + value + "\n");

        int size = intLinkedList.size();
        System.out.println("int size(): " + size + "\n");

        IntList sub = intLinkedList.subList(0, 4);
        System.out.println("subList(0, 4): " + sub + "\n");

        int[] array = sub.toArray();
        System.out.println("toArray: " + Arrays.toString(array) + "\n");

        int remove = intLinkedList.remove();
        System.out.println("int remove(): " + intLinkedList + " int remove = " + remove + "\n");

        boolean set = intLinkedList.set(3, 66);
        System.out.println("boolean set(3, 66) = " + set);
        System.out.println(intLinkedList + "\n");

        int element = intLinkedList.element();
        System.out.println("element(): " + intLinkedList + " int element = " + element + "\n");

        int pop = intLinkedList.pop();
        System.out.println("pop(): " + intLinkedList + " int pop = " + pop + "\n");

        int peek = intLinkedList.peek();
        System.out.println("peek(): " + intLinkedList + " int peek = " + peek + "\n");

        intLinkedList.clear();
        System.out.println("void clear(): " + intLinkedList);
    }
}
