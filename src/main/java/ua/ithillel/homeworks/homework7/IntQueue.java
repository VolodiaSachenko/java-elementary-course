package ua.ithillel.homeworks.homework7;

public interface IntQueue {
    int remove(); // return first in Queue and remove it

    int element(); // return first in Queue but not remove it
}
