package ua.ithillel.homeworks.homework7;

import java.util.*;

/**
 * @author Volodia Sachenko & Josh Bloch
 */

public class IntLinkedList implements IntList, IntQueue, IntStack {

    private Item first;
    private Item last;
    private transient int size = 0;

    public IntLinkedList() {

    }

    @Override
    public void add(int value) {
        final Item l = last;
        final Item newElement = new Item(l, value, null);
        last = newElement;
        if (l == null) {
            first = newElement;
        } else {
            l.next = newElement;
        }
        size++;
    }

    @Override
    public boolean add(int index, int value) {
        checkPositionIndex(index);
        if (index == size) {
            add(value);
            return false;
        } else {
            linkBefore(value, node(index));
            return true;
        }
    }

    @Override
    public void clear() {
        for (Item x = first; x != null; ) {
            Item next = x.next;
            x.value = 0;
            x.next = null;
            x.prev = null;
            x = next;
        }
        first = last = null;
        size = 0;
    }

    @Override
    public int get(int index) {
        checkElementIndex(index);
        return node(index).value;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean remove(int index) {
        if (index < 0 || index >= size) {
            return false;
        }
        if (index == 0) {
            unlinkFirst(node(index));
            return true;
        }
        if (index == size - 1) {
            unlinkLast(node(index));
        } else {
            unlink(node(index));
        }
        return true;
    }

    @Override
    public boolean removeByValue(int value) {
        for (Item x = first; x != null; x = x.next) {
            if (x.value == value) {
                unlink(x);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, int value) {
        return add(index, value);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public IntList subList(int fromIndex, int toIndex) {
        subListRangeCheck(fromIndex, toIndex, size());
        IntList intLinkedList = new IntLinkedList();
        int i = fromIndex;
        while (i < toIndex) {
            intLinkedList.add(get(i++));
        }
        return intLinkedList;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[size];
        int i = 0;
        Item newItem = first;
        while (i < size) {
            result[i] = newItem.value;
            newItem = newItem.next;
            ++i;
        }
        return result;
    }

    @Override
    public int remove() {
        final Item f = first;
        if (f == null) {
            {
                throw new NoSuchElementException();
            }
        }
        int remove = first.value;
        unlinkFirst(f);
        return remove;
    }

    @Override
    public int element() {
        return first.value;
    }

    @Override
    public int pop() {
        return remove();
    }

    @Override
    public int peek() {
        final Item f = first;
        return (f == null) ? 0 : f.value;
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "[]";
        } else {
            int[] itemArray = this.toArray();
            return Arrays.toString(itemArray);
        }
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    static void subListRangeCheck(int fromIndex, int toIndex, int size) {
        if (fromIndex < 0) {
            throw new IndexOutOfBoundsException("fromIndex = " + fromIndex);
        }
        if (toIndex > size) {
            throw new IndexOutOfBoundsException("toIndex = " + toIndex);
        }
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException("fromIndex(" + fromIndex +
                    ") > toIndex(" + toIndex + ")");
        }
    }

    Item node(int index) {
        Item x;
        if (index < (size >> 1)) {
            x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
        } else {
            x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
        }
        return x;
    }

    void linkBefore(int value, Item item) {
        final Item pred = item.prev;
        final Item newNode = new Item(pred, value, item);
        item.prev = newNode;
        if (pred == null) {
            first = newNode;
        } else {
            pred.next = newNode;
        }
        size++;
    }

    private void unlinkFirst(Item f) {
        final Item next = f.next;
        f.value = 0;
        f.next = null;
        first = next;
        if (next == null) {
            last = null;
        } else {
            next.prev = null;
        }
        size--;
    }

    private void unlinkLast(Item l) {
        final Item prev = l.prev;
        l.value = 0;
        l.prev = null;
        last = prev;
        if (prev == null) {
            first = null;
        } else {
            prev.next = null;
        }
        size--;
    }

    void unlink(Item x) {
        final Item next = x.next;
        final Item prev = x.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.value = 0;
        size--;
    }

    private static class Item {
        int value;
        Item next;
        Item prev;

        Item(Item prev, int value, Item next) {
            this.value = value;
            this.next = next;
            this.prev = prev;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}
