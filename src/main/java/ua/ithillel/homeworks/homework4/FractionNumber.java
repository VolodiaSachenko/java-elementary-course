package ua.ithillel.homeworks.homework4;

public final class FractionNumber {
    private final int numerator;
    private final int denominator;

    public FractionNumber(int numerator) {
        this.numerator = numerator;
        this.denominator = 1;
    }

    public FractionNumber(int numerator, int denominator) {
        int gcd = calculateGCD(numerator, denominator);
        if (denominator == 0) {
            throw new IllegalArgumentException("Error, denominator is zero");
        }
        if (numerator < 0 || denominator < 0) {
            numerator *= -1;
            denominator *= -1;
        }

        this.numerator = numerator / gcd;
        this.denominator = denominator / gcd;
    }

    public FractionNumber plus(FractionNumber number) {
        int newNumerator = (numerator * number.getDenominator()) +
                (number.getNumerator() * denominator);
        int newDenominator = denominator * number.getDenominator();
        return new FractionNumber(newNumerator, newDenominator);
    }

    public FractionNumber minus(FractionNumber number) {
        int newNumerator = (numerator * number.denominator) - (number.numerator * denominator);
        int newDenominator = denominator * number.denominator;
        return new FractionNumber(newNumerator, newDenominator);
    }

    public FractionNumber multiply(FractionNumber number) {
        int newNumerator = numerator * number.numerator;
        int newDenominator = denominator * number.denominator;
        return new FractionNumber(newNumerator, newDenominator);
    }

    public FractionNumber divide(FractionNumber number) {
        int newNumerator = numerator * number.getDenominator();
        int newDenominator = denominator * number.numerator;
        return new FractionNumber(newNumerator, newDenominator);
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public int calculateGCD(int numerator, int denominator) {
        if (numerator % denominator == 0) {
            return denominator;
        }
        return calculateGCD(denominator, numerator % denominator);
    }

    @Override
    public String toString() {
        if (this.numerator == 0) {
            return "0";
        }
        return this.numerator + "/" +
                this.denominator;
    }
}
