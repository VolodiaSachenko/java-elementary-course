package ua.ithillel.homeworks.homework4;

/**
 * @author Volodia Sachenko
 */

public class Homework4 {
    public static void main(String[] args) {
        FractionNumber fractionNumber = new FractionNumber(3, 4);
        System.out.println("Whole number: " + new FractionNumber(4));
        System.out.println("If numerator = 0, the result is: " + new FractionNumber(0, 1));
        System.out.println("Method public FractionNumber plus(FractionNumber number): " + fractionNumber.plus
                (new FractionNumber(2, 3)));
        System.out.println("Method public FractionNumber minus(FractionNumber number): " + fractionNumber.minus
                (new FractionNumber(2, 4)));
        System.out.println("Method public FractionNumber multiply(FractionNumber number): " + fractionNumber.multiply
                (new FractionNumber(3, 5)));
        System.out.println("Method public FractionNumber divide(FractionNumber number): " + fractionNumber.divide
                (new FractionNumber(4, 3)));

        try {
            System.out.println("Method public FractionNumber divide(FractionNumber number): " + fractionNumber.divide
                    (new FractionNumber(2, 0)));
        } catch (Exception e) {
            System.out.println("Error, denominator is zero");
        }
    }
}
