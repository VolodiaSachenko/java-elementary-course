package ua.ithillel.homeworks.homework9;

import java.math.BigInteger;

public class Fibonacci {

    public static BigInteger fibonacciNumbers(int n) {
        BigInteger[] numbers = new BigInteger[n + 1];
        numbers[0] = BigInteger.valueOf(0);
        numbers[1] = BigInteger.valueOf(1);

        for (int i = 2; i <= n; i++) {
            numbers[i] = numbers[i - 1].add(numbers[i - 2]);
        }
        return numbers[n];
    }
}
