package ua.ithillel.homeworks.homework9;

public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }

    public void printTree(String prefix, TreeNode n) {
        if (n != null) {
            System.out.println(prefix + "|-- " + n.getVal());
            printTree(prefix + "    ", n.getRight());
            printTree(prefix + "    ", n.getLeft());
        }
    }

    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null) {
            return t2;
        }
        if (t2 == null) {
            return t1;
        }
        t1.setVal(t1.getVal() + t2.getVal());
        t1.setLeft(mergeTrees(t1.getLeft(), t2.getLeft()));
        t1.setRight(mergeTrees(t1.getRight(), t2.getRight()));
        return t1;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }
}
