package ua.ithillel.homeworks.homework9;

/**
 * @author Volodia Sachenko
 */

public class Homework9 {
    public static void main(String[] args) {
        System.out.println("Fibonacci numbers: ");
        System.out.println("F(10): " + Fibonacci.fibonacciNumbers(10));
        System.out.println("F(38): " + Fibonacci.fibonacciNumbers(38));
        System.out.println("F(71): " + Fibonacci.fibonacciNumbers(71) + "\n");

        TreeNode tree1 = new TreeNode(1);
        tree1.setLeft(new TreeNode(3));
        tree1.setRight(new TreeNode(2));
        tree1.getLeft().setLeft(new TreeNode(5));

        TreeNode tree2 = new TreeNode(2);
        tree2.setLeft(new TreeNode(1));
        tree2.getLeft().setRight(new TreeNode(4));
        tree2.setRight(new TreeNode(3));
        tree2.getRight().setRight(new TreeNode(7));

        TreeNode mergedTree = tree1.mergeTrees(tree1, tree2);
        System.out.println("The merged tree:");
        mergedTree.printTree(" ", mergedTree);
    }
}
