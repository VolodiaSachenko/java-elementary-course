package ua.ithillel.homeworks.homework8;

/**
 * @author Volodia Sachenko
 */

public class Homework8 {
    public static void main(String[] args) {
        CountDuplicates.countDuplicates(20);
        System.out.println();

        Node root = new Node("root");
        Node child = new Node("child");
        Node child2 = new Node("child2");
        Node child3 = new Node("child3");
        Node child4 = new Node("child4");
        Node child5 = new Node("child5");
        Node child6 = new Node("child6");
        Node child7 = new Node("child7");
        Node child8 = new Node("child8");
        Node child9 = new Node("child9");

        root.insert(child, "root");
        root.insert(child2, "root");
        root.insert(child3, "root");
        root.insert(child4, "child");
        root.insert(child5, "child");
        root.insert(child6, "child2");
        root.insert(child7, "child2");
        root.insert(child8, "child3");
        root.insert(child9, "child3");
        System.out.println(root.printTree(0));
    }
}
