package ua.ithillel.homeworks.homework8;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private final String name;
    private final List<Node> children;

    public Node(String name) {
        this.name = name;
        this.children = new ArrayList<>();
    }

    public void insert(Node newNode, String parentName) {
        if (parentName.equals(getName())) {
            getChildren().add(newNode);
            return;
        }
        for (Node child : getChildren()) {
            child.insert(newNode, parentName);
        }
    }

    public String printTree(int level) {
        StringBuilder print = new StringBuilder();
        if (level > 0) {
            print.append('`');
        }
        print.append(" - ".repeat(Math.max(0, level)));
        print.append(getName()).append(System.lineSeparator());
        for (Node child : getChildren()) {
            print.append(child.printTree(level + 1));
        }
        return print.toString();
    }

    public String getName() {
        return name;
    }

    public List<Node> getChildren() {
        return children;
    }
}
