package ua.ithillel.homeworks.homework8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CountDuplicates {
    public static void countDuplicates(int size) {
        List<Integer> list = randomNumbers(size);
        Map<Integer, Integer> matches = putArrayList(list);
        System.out.println(list);
        for (Map.Entry<Integer, Integer> entry : matches.entrySet()) {
            if (entry.getValue() > 1) {
                System.out.println("Цифра: " + entry.getKey() + ", повторів: " + entry.getValue());
            }
        }
    }

    private static List<Integer> randomNumbers(int size) {
        List<Integer> random = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            random.add((int) (1 + Math.random() * 100));
        }
        return random;
    }

    private static Map<Integer, Integer> putArrayList(List<Integer> list) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer integer : list) {
            if (!map.containsKey(integer)) {
                map.put(integer, 1);
            } else {
                map.put(integer, map.get(integer) + 1);
            }
        }
        return map;
    }
}
